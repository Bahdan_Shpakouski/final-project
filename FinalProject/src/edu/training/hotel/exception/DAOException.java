package edu.training.hotel.exception;

import java.sql.SQLException;

public class DAOException extends SQLException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1920210218766026765L;

	public DAOException() {	}

	public DAOException(String reason) {
		super(reason);
	}

	public DAOException(Throwable cause) {
		super(cause);
	}

	public DAOException(String reason, String SQLState) {
		super(reason, SQLState);
	}

	public DAOException(String reason, Throwable cause) {
		super(reason, cause);
	}

	public DAOException(String reason, String SQLState, int vendorCode) {
		super(reason, SQLState, vendorCode);
	}

	public DAOException(String reason, String sqlState, Throwable cause) {
		super(reason, sqlState, cause);
	}

	public DAOException(String reason, String sqlState, int vendorCode, Throwable cause) {
		super(reason, sqlState, vendorCode, cause);
	}

}
