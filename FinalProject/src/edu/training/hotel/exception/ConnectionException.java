package edu.training.hotel.exception;

import java.sql.SQLException;

public class ConnectionException extends SQLException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5614023890647339810L;

	public ConnectionException() {
		super();
	}

	public ConnectionException(String reason, String sqlState, int vendorCode, Throwable cause) {
		super(reason, sqlState, vendorCode, cause);
	}

	public ConnectionException(String reason, String SQLState, int vendorCode) {
		super(reason, SQLState, vendorCode);
	}

	public ConnectionException(String reason, String sqlState, Throwable cause) {
		super(reason, sqlState, cause);
	}

	public ConnectionException(String reason, String SQLState) {
		super(reason, SQLState);
	}

	public ConnectionException(String reason, Throwable cause) {
		super(reason, cause);
	}

	public ConnectionException(String reason) {
		super(reason);
	}

	public ConnectionException(Throwable cause) {
		super(cause);
	}


}
