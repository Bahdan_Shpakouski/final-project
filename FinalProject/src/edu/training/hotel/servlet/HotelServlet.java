package edu.training.hotel.servlet;

import java.io.IOException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import edu.training.hotel.command.HotelCommand;
import edu.training.hotel.command.HotelCommandFactory;
import edu.training.hotel.exception.CommandException;

/**
 * Servlet implementation class HotelServlet
 */
@WebServlet("/HotelServlet")
public class HotelServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	private static final String COMMAND_PARAMETR = "command";
	private static final String ERROR_PAGE = "WEB-INF/pages/errorPage.jsp";
	
	static Logger logger;
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public HotelServlet() {
        super();
    }

	/**
	 * @see Servlet#init(ServletConfig)
	 */
	public void init(ServletConfig config) throws ServletException {	
		logger = Logger.getLogger(HotelServlet.class);
		logger.info("init");
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) 
					throws ServletException, IOException {
		process(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) 
					throws ServletException, IOException {
		process(request, response);
	}

	private void process(HttpServletRequest request,
			HttpServletResponse response) 
					throws ServletException, IOException{
		logger.info("command= "+request.getParameter(COMMAND_PARAMETR));
		HotelCommandFactory factory = new HotelCommandFactory();	
		String page;
		try {
			HotelCommand command = factory.getCommand(request);
			page = command.process(request);
		} catch (CommandException e) {
			e.printStackTrace();
			logger.info(e.getMessage());
			page = ERROR_PAGE;
		} catch (Exception e){
			e.printStackTrace();
			logger.info("�������������� ������" + e.getMessage());
			page = ERROR_PAGE;
		}
		request.getRequestDispatcher(page).forward(request, response);
	}
	
	@Override
	public void destroy() {
    	logger.info("destroy");
		super.destroy();
	}

}
