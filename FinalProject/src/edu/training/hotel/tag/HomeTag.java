package edu.training.hotel.tag;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

public class HomeTag extends TagSupport {

	/**
	 * 
	 */
	private static final long serialVersionUID = 9003611107919365774L;

	public HomeTag() {
		
	}

	public int doStartTag() throws JspException{
		String home = "<h3 align='center'><a href='index.jsp'>Home</a></h3>";
		try{
			JspWriter out = pageContext.getOut();
			out.write(home);
		}catch(IOException e){
			throw new JspException(e);
		}
		return SKIP_BODY;
	}
	
	public int doEndTag(){
		return EVAL_PAGE;
	}
}
