package edu.training.hotel.dao;

import java.util.ArrayList;

import edu.training.hotel.entity.Apartment;
import edu.training.hotel.exception.ConnectionException;
import edu.training.hotel.exception.DAOException;;

public interface ApartmentDAO {

	ArrayList<Apartment> selectApartments() 
			throws DAOException, ConnectionException;
	
	Apartment selectApartment(long id) 
			throws DAOException, ConnectionException;
	
	void updateAdditionalInformation(long id, boolean hasTV,
			boolean hasFridge, boolean hasAirConditioner) 
					throws DAOException, ConnectionException;
	
}
