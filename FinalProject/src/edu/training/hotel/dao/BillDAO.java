package edu.training.hotel.dao;

import java.util.ArrayList;

import edu.training.hotel.entity.Bill;
import edu.training.hotel.exception.ConnectionException;
import edu.training.hotel.exception.DAOException;

public interface BillDAO {
	
	ArrayList<Bill> selectBills() throws DAOException,
		ConnectionException;
	ArrayList<Bill> selectNewBills() throws DAOException,
		ConnectionException;
	ArrayList<Bill> selectClientBills(long clientId) throws DAOException,
		ConnectionException;
	Bill selectBill(long id) throws DAOException, ConnectionException;
	void createBill(Bill bill) throws DAOException,
		ConnectionException;
	void updateBill(long id, Bill bill) throws DAOException,
			ConnectionException;
	void deleteBill(long id) throws DAOException, ConnectionException;
}
