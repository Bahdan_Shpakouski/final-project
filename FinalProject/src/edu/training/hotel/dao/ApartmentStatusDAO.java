package edu.training.hotel.dao;

import java.util.ArrayList;
import java.util.Date;

import edu.training.hotel.entity.ApartmentStatus;
import edu.training.hotel.exception.ConnectionException;
import edu.training.hotel.exception.DAOException;;

public interface ApartmentStatusDAO {

	ArrayList<ApartmentStatus> selectCurrentStatuses() throws DAOException,
			ConnectionException;
	
	ArrayList<ApartmentStatus> selectReservedStatuses() throws DAOException,
	ConnectionException;
	
	ArrayList<ApartmentStatus> selectExpiredStatuses() throws DAOException,
	ConnectionException;
	
	ApartmentStatus selectStatus(long id) throws DAOException,
			ConnectionException;
	
	ApartmentStatus selectStatusForApartment(long apartmentId,
			Date arrivalDate,Date departureDate)
					throws DAOException, ConnectionException;
	
	void createStatus(ApartmentStatus status) throws DAOException,
			ConnectionException;
	
	void updateStatus(long id, ApartmentStatus status) 
			throws DAOException, ConnectionException;
	
	void deleteStatus(long id) throws DAOException, ConnectionException;
	
	void setCurrent(long id, boolean current) throws DAOException, ConnectionException;
}
