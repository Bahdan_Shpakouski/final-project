package edu.training.hotel.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import edu.training.hotel.connectionpool.ProxyConnection;
import edu.training.hotel.connectionpool.ProxyConnectionPool;
import edu.training.hotel.entity.Bill;
import edu.training.hotel.exception.ConnectionException;
import edu.training.hotel.exception.DAOException;

public class BillDAOImpl implements BillDAO {

	private static final String SELECT_ALL = "select * from hotel.bills;";
	private static final String SELECT_ALL_NEW = "select * from hotel.bills"
			+ " where bills.bill_status != 'Confirmed';";
	private static final String SELECT_ALL_FOR_CLIENT = "select * from"
			+ " hotel.bills where client_id = ?;";
	private static final String FIND = "select * from hotel.bills "
			+ "where bill_id = ?;";
	private static final String INSERT = "insert into hotel.bills (cost,"
			+ " moving_in_date, moving_out_date, bill_status, client_id, "
			+ "apartment_id)values (?, ?, ?, ?, ?, ?);";
	private static final String UPDATE = "update hotel.bills set cost = ?,"
			+ " moving_in_date = ?, moving_out_date = ?, bill_status = ?,"
			+ " client_id = ?, apartment_id = ? where bill_id = ?;";
	private static final String DELETE = "delete from hotel.bills "
			+ "where bill_id = ?;";
	private static final String STATEMENT_ERROR_MESSAGE = "������ ��� ��������"
			+ " ������� prepaired statement";
	private static final String CONNECTION_ERROR_MESSAGE = "������ ��� "
			+ "�������� ������� connection";
	
	static Logger logger = Logger.getLogger(BillDAOImpl.class);
	
	public BillDAOImpl() {
		
	}

	@Override
	public ArrayList<Bill> selectBills() throws DAOException,
			ConnectionException {
		ArrayList<Bill> list = new ArrayList<Bill>();
		ProxyConnection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try{ 
			conn = ProxyConnectionPool.getInstance().getConnection();
			ps = conn.prepareStatement(SELECT_ALL);
			rs = ps.executeQuery();
			while(rs.next()){
				Bill bill = new Bill();
				bill.setBillId(rs.getLong(1));
				bill.setCost(rs.getInt(2));
				bill.setMovingInDate(rs.getDate(3));
				bill.setMovingOutDate(rs.getDate(4));
				bill.setBillStatus(rs.getString(5));
				bill.setClientId(rs.getLong(6));
				bill.setApartmentId(rs.getLong(7));
				list.add(bill);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		}finally{
			try {
				ps.close();
			}catch (SQLException e) {
				logger.error(STATEMENT_ERROR_MESSAGE);
			}
			try{
				conn.close();
			} catch (SQLException e) {
				logger.error(CONNECTION_ERROR_MESSAGE);
			}
		}
		return list;
	}
	
	@Override
	public ArrayList<Bill> selectNewBills() throws DAOException,
			ConnectionException {
		ArrayList<Bill> list = new ArrayList<Bill>();
		ProxyConnection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try{ 
			conn = ProxyConnectionPool.getInstance().getConnection();
			ps = conn.prepareStatement(SELECT_ALL_NEW);
			rs = ps.executeQuery();
			while(rs.next()){
				Bill bill = new Bill();
				bill.setBillId(rs.getLong(1));
				bill.setCost(rs.getInt(2));
				bill.setMovingInDate(rs.getDate(3));
				bill.setMovingOutDate(rs.getDate(4));
				bill.setBillStatus(rs.getString(5));
				bill.setClientId(rs.getLong(6));
				bill.setApartmentId(rs.getLong(7));
				list.add(bill);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		}finally{
			try {
				ps.close();
			}catch (SQLException e) {
				logger.error(STATEMENT_ERROR_MESSAGE);
			}
			try{
				conn.close();
			} catch (SQLException e) {
				logger.error(CONNECTION_ERROR_MESSAGE);
			}
		}
		return list;
	}
	
	@Override
	public ArrayList<Bill> selectClientBills(long clientId) throws DAOException,
			ConnectionException{
		ArrayList<Bill> list = new ArrayList<Bill>();
		ProxyConnection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try{ 
			conn = ProxyConnectionPool.getInstance().getConnection();
			ps = conn.prepareStatement(SELECT_ALL_FOR_CLIENT);
			ps.setLong(1, clientId);
			rs = ps.executeQuery();
			while(rs.next()){
				Bill bill = new Bill();
				bill.setBillId(rs.getLong(1));
				bill.setCost(rs.getInt(2));
				bill.setMovingInDate(rs.getDate(3));
				bill.setMovingOutDate(rs.getDate(4));
				bill.setBillStatus(rs.getString(5));
				bill.setClientId(rs.getLong(6));
				bill.setApartmentId(rs.getLong(7));
				list.add(bill);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		}finally{
			try {
				ps.close();
			}catch (SQLException e) {
				logger.error(STATEMENT_ERROR_MESSAGE);
			}
			try{
				conn.close();
			} catch (SQLException e) {
				logger.error(CONNECTION_ERROR_MESSAGE);
			}
		}
		return list;
	}

	@Override
	public Bill selectBill(long id) throws DAOException, ConnectionException {
		ProxyConnection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Bill bill = new Bill();
		try{ 
			conn = ProxyConnectionPool.getInstance().getConnection();
			ps = conn.prepareStatement(FIND);
			ps.setLong(1, id);
			rs = ps.executeQuery();
			while(rs.next()){
				bill.setBillId(rs.getLong(1));
				bill.setCost(rs.getInt(2));
				bill.setMovingInDate(rs.getDate(3));
				bill.setMovingOutDate(rs.getDate(4));
				bill.setBillStatus(rs.getString(5));
				bill.setClientId(rs.getLong(6));
				bill.setApartmentId(rs.getLong(7));
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		}finally{
			try {
				ps.close();
			}catch (SQLException e) {
				logger.error(STATEMENT_ERROR_MESSAGE);
			}
			try{
				conn.close();
			} catch (SQLException e) {
				logger.error(CONNECTION_ERROR_MESSAGE);
			}
		}
		return bill;
	}

	@Override
	public void createBill(Bill bill) throws DAOException,
			ConnectionException {
		ProxyConnection conn = null;
		PreparedStatement ps = null;
		try{ 
			conn = ProxyConnectionPool.getInstance().getConnection();
			ps = conn.prepareStatement(INSERT);
			ps.setInt(1, bill.getCost());
			ps.setDate(2, new java.sql.Date(bill.getMovingInDate().getTime()));
			ps.setDate(3, new java.sql.Date(bill.getMovingOutDate().getTime()));
			ps.setString(4, bill.getBillStatus());
			ps.setLong(5, bill.getClientId());
			ps.setLong(6, bill.getApartmentId());
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		}finally{
			try {
				ps.close();
			}catch (SQLException e) {
				logger.error(STATEMENT_ERROR_MESSAGE);
			}
			try{
				conn.close();
			} catch (SQLException e) {
				logger.error(CONNECTION_ERROR_MESSAGE);
			}
		}
	}

	@Override
	public void updateBill(long id, Bill bill) throws DAOException,
			ConnectionException {
		ProxyConnection conn = null;
		PreparedStatement ps = null;
		try{ 
			conn = ProxyConnectionPool.getInstance().getConnection();
			ps = conn.prepareStatement(UPDATE);
			ps.setInt(1, bill.getCost());
			ps.setDate(2, new java.sql.Date(bill.getMovingInDate().getTime()));
			ps.setDate(3, new java.sql.Date(bill.getMovingOutDate().getTime()));
			ps.setString(4, bill.getBillStatus());
			ps.setLong(5, bill.getClientId());
			ps.setLong(6, bill.getApartmentId());
			ps.setLong(7, id);
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		}finally{
			try {
				ps.close();
			}catch (SQLException e) {
				logger.error(STATEMENT_ERROR_MESSAGE);
			}
			try{
				conn.close();
			} catch (SQLException e) {
				logger.error(CONNECTION_ERROR_MESSAGE);
			}
		}	
	}

	@Override
	public void deleteBill(long id) throws DAOException, ConnectionException {
		ProxyConnection conn = null;
		PreparedStatement ps = null;
		try{ 
			conn = ProxyConnectionPool.getInstance().getConnection();
			ps = conn.prepareStatement(DELETE);
			ps.setLong(1, id);
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		}finally{
			try {
				ps.close();
			}catch (SQLException e) {
				logger.error(STATEMENT_ERROR_MESSAGE);
			}
			try{
				conn.close();
			} catch (SQLException e) {
				logger.error(CONNECTION_ERROR_MESSAGE);
			}
		}
	}
}
