package edu.training.hotel.dao;

import java.util.ArrayList;

import edu.training.hotel.entity.Application;
import edu.training.hotel.exception.ConnectionException;
import edu.training.hotel.exception.DAOException;

public interface ApplicationDAO {

	ArrayList<Application> selectApplications() 
			throws DAOException, ConnectionException;
	
	ArrayList<Application> selectApplications(long clientId) 
			throws DAOException, ConnectionException;
	
	ArrayList<Application> selectNewApplications() 
			throws DAOException, ConnectionException;
	
	Application selectApplication(long id) throws DAOException,
			ConnectionException;
	
	void createApplication(Application application) 
			throws DAOException, ConnectionException;
	
	void updateApplication(long id, Application application)
			throws DAOException, ConnectionException;
	
	void deleteApplication(long id) throws DAOException,
			ConnectionException;
}
