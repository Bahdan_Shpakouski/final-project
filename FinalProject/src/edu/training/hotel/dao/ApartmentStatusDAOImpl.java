package edu.training.hotel.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;

import org.apache.log4j.Logger;

import edu.training.hotel.connectionpool.ProxyConnection;
import edu.training.hotel.connectionpool.ProxyConnectionPool;
import edu.training.hotel.entity.ApartmentStatus;
import edu.training.hotel.exception.ConnectionException;
import edu.training.hotel.exception.DAOException;

public class ApartmentStatusDAOImpl implements ApartmentStatusDAO {

	private static final String SELECT_CURRENT = "select * from "
			+ "hotel.apartment_status where current = true;";
	private static final String SELECT_RESERVED = "select * from "
			+ "hotel.apartment_status where status = 'Reserved';";
	private static final String SELECT_EXPIRED = "select * from "
			+ "hotel.apartment_status where status = 'Expired';";
	private static final String FIND = "select * from hotel.apartment_status "
			+ "where status_id = ?;";
	private static final String INSERT = "insert into hotel.apartment_status "
			+ "(status, current, apartment_id, bill_id, client_id) "
			+ "values ( ?, ?, ?, ?, ?);";
	private static final String UPDATE = "update hotel.apartment_status "
			+ "set status = ?, current = ?, apartment_id = ?, bill_id = ?,"
			+ " client_id = ? where status_id = ?;";
	private static final String DELETE = "delete from hotel.apartment_status "
			+ "where status_id = ?;";
	private static final String FIND_RESERVED = "select status_id, status, "
			+ "current, apartment_status.apartment_id, "
			+ "apartment_status.bill_id, apartment_status.client_id, "
			+ "moving_in_date from hotel.apartment_status, hotel.bills "
			+ "where apartment_status.apartment_id = ?  "
			+ "&& bills.bill_id = apartment_status.bill_id "
			+ "&& ((moving_in_date <= ? && moving_out_date >= ?) "
			+ "|| (moving_in_date <= ? && moving_out_date >= ?) "
			+ "|| (moving_in_date > ? && moving_out_date < ?)) "
			+ "&& status != 'Expired';";
	private static final String STATEMENT_ERROR_MESSAGE = "������ ��� ��������"
			+ " ������� prepaired statement";
	private static final String CONNECTION_ERROR_MESSAGE = "������ ��� "
			+ "�������� ������� connection";
	private static final String SET_CURRENT = "update hotel.apartment_status"
			+ " set current = ? where apartment_id = ? && client_id is null; ";
	
	static Logger logger = Logger.getLogger(ApartmentStatusDAOImpl.class);
	
	public ApartmentStatusDAOImpl() {

	}

	@Override
	public ArrayList<ApartmentStatus> selectCurrentStatuses() 
			throws DAOException, ConnectionException {
		ArrayList<ApartmentStatus> list = new ArrayList<ApartmentStatus>();
		ProxyConnection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try{
			conn = ProxyConnectionPool.getInstance().getConnection();
			ps = conn.prepareStatement(SELECT_CURRENT);
			rs = ps.executeQuery();
			while(rs.next()){
				ApartmentStatus status = new ApartmentStatus();
				status.setStatusId(rs.getLong(1));
				status.setStatus(rs.getString(2));
				status.setCurrent(rs.getBoolean(3));
				status.setApartmentId(rs.getLong(4));
				status.setBillId(rs.getLong(5));
				status.setClientId(rs.getLong(6));
				list.add(status);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		}finally{
			try {
				ps.close();
			}catch (SQLException e) {
				logger.error(STATEMENT_ERROR_MESSAGE);
			}
			try{
				conn.close();
			} catch (SQLException e) {
				logger.error(CONNECTION_ERROR_MESSAGE);
			}
		}
		return list;
	}

	@Override
	public ApartmentStatus selectStatus(long id) throws DAOException,
			ConnectionException {
		ProxyConnection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		ApartmentStatus status = new ApartmentStatus();
		try{
			conn = ProxyConnectionPool.getInstance().getConnection();
			ps = conn.prepareStatement(FIND);
			ps.setLong(1, id);
			rs = ps.executeQuery();
			while(rs.next()){				
				status.setStatusId(rs.getLong(1));
				status.setStatus(rs.getString(2));
				status.setCurrent(rs.getBoolean(3));
				status.setApartmentId(rs.getInt(4));
				status.setBillId(rs.getInt(5));
				status.setClientId(rs.getInt(6));
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		}finally{
			try {
				ps.close();
			}catch (SQLException e) {
				logger.error(STATEMENT_ERROR_MESSAGE);
			}
			try{
				conn.close();
			} catch (SQLException e) {
				logger.error(CONNECTION_ERROR_MESSAGE);
			}
		}
		return status;
	}

	@Override
	public void createStatus(ApartmentStatus status) 
			throws DAOException, ConnectionException {
		ProxyConnection conn = null;
		PreparedStatement ps = null;
		try{ 
			conn = ProxyConnectionPool.getInstance().getConnection();
			ps = conn.prepareStatement(INSERT);
			ps.setString(1, status.getStatus());
			ps.setBoolean(2, status.isCurrent());
			ps.setLong(3, status.getApartmentId());
			if(status.getBillId() == 0){
				ps.setNull(4, java.sql.Types.INTEGER);
			} else{
				ps.setLong(4, status.getBillId());
			}
			if(status.getClientId() == 0){
				ps.setNull(5, java.sql.Types.INTEGER);
			} else{
				ps.setLong(5, status.getClientId());
			}
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		}finally{
			try {
				ps.close();
			}catch (SQLException e) {
				logger.error(STATEMENT_ERROR_MESSAGE);
			}
			try{
				conn.close();
			} catch (SQLException e) {
				logger.error(CONNECTION_ERROR_MESSAGE);
			}
		}
	}

	@Override
	public void updateStatus(long id, ApartmentStatus status) 
			throws DAOException, ConnectionException {
		ProxyConnection conn = null;
		PreparedStatement ps = null;
		try{
			conn = ProxyConnectionPool.getInstance().getConnection();
			ps = conn.prepareStatement(UPDATE);
			ps.setString(1, status.getStatus());
			ps.setBoolean(2, status.isCurrent());
			ps.setLong(3, status.getApartmentId());
			if(status.getBillId() == 0){
				ps.setNull(4, java.sql.Types.INTEGER);
			} else{
				ps.setLong(4, status.getBillId());
			}
			if(status.getClientId() == 0){
				ps.setNull(5, java.sql.Types.INTEGER);
			} else{
				ps.setLong(5, status.getClientId());
			}
			ps.setLong(6, id);
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		}finally{
			try {
				ps.close();
			}catch (SQLException e) {
				logger.error(STATEMENT_ERROR_MESSAGE);
			}
			try{
				conn.close();
			} catch (SQLException e) {
				logger.error(CONNECTION_ERROR_MESSAGE);
			}
		}
	}

	@Override
	public void deleteStatus(long id) throws DAOException,
			ConnectionException {
		ProxyConnection conn = null;
		PreparedStatement ps = null;
		try{
			conn = ProxyConnectionPool.getInstance().getConnection();
			ps = conn.prepareStatement(DELETE);
			ps.setLong(1, id);
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		}finally{
			try {
				ps.close();
			}catch (SQLException e) {
				logger.error(STATEMENT_ERROR_MESSAGE);
			}
			try{
				conn.close();
			} catch (SQLException e) {
				logger.error(CONNECTION_ERROR_MESSAGE);
			}
		}
	}

	@Override
	public ApartmentStatus selectStatusForApartment(long apartmentId,
			Date arrivalDate, Date departureDate)
			throws DAOException, ConnectionException {
		ApartmentStatus status = null;
		ProxyConnection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Date statusDate = null;
		java.sql.Date date1= new java.sql.Date(arrivalDate.getTime());
		java.sql.Date date2= new java.sql.Date(departureDate.getTime());
		try{
			conn = ProxyConnectionPool.getInstance().getConnection();
			ps = conn.prepareStatement(FIND_RESERVED);
			ps.setLong(1, apartmentId);
			ps.setDate(2, date1);
			ps.setDate(3, date1);
			ps.setDate(4, date2);
			ps.setDate(5, date2);
			ps.setDate(6, date1);
			ps.setDate(7, date2);
			rs = ps.executeQuery();
			while(rs.next()){
				ApartmentStatus temp = new ApartmentStatus();
				temp.setStatusId(rs.getLong(1));
				temp.setStatus(rs.getString(2));
				temp.setCurrent(rs.getBoolean(3));
				temp.setApartmentId(rs.getLong(4));
				temp.setBillId(rs.getLong(5));
				temp.setClientId(rs.getLong(6));
				Date tempDate = rs.getDate(7);
				if(status == null || tempDate.before(statusDate)){
					statusDate = tempDate;
					status = temp;
				}
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		}finally{
			try {
				ps.close();
			}catch (SQLException e) {
				logger.error(STATEMENT_ERROR_MESSAGE);
			}
			try{
				conn.close();
			} catch (SQLException e) {
				logger.error(CONNECTION_ERROR_MESSAGE);
			}
		}
		return status;
	}

	@Override
	public ArrayList<ApartmentStatus> selectReservedStatuses() 
			throws DAOException, ConnectionException {
		ArrayList<ApartmentStatus> list = new ArrayList<ApartmentStatus>();
		ProxyConnection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try{
			conn = ProxyConnectionPool.getInstance().getConnection();
			ps = conn.prepareStatement(SELECT_RESERVED);
			rs = ps.executeQuery();
			while(rs.next()){
				ApartmentStatus status = new ApartmentStatus();
				status.setStatusId(rs.getLong(1));
				status.setStatus(rs.getString(2));
				status.setCurrent(rs.getBoolean(3));
				status.setApartmentId(rs.getLong(4));
				status.setBillId(rs.getLong(5));
				status.setClientId(rs.getLong(6));
				list.add(status);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		}finally{
			try {
				ps.close();
			}catch (SQLException e) {
				logger.error(STATEMENT_ERROR_MESSAGE);
			}
			try{
				conn.close();
			} catch (SQLException e) {
				logger.error(CONNECTION_ERROR_MESSAGE);
			}
		}
		return list;
	}

	@Override
	public ArrayList<ApartmentStatus> selectExpiredStatuses() 
			throws DAOException, ConnectionException {
		ArrayList<ApartmentStatus> list = new ArrayList<ApartmentStatus>();
		ProxyConnection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try{
			conn = ProxyConnectionPool.getInstance().getConnection();
			ps = conn.prepareStatement(SELECT_EXPIRED);
			rs = ps.executeQuery();
			while(rs.next()){
				ApartmentStatus status = new ApartmentStatus();
				status.setStatusId(rs.getLong(1));
				status.setStatus(rs.getString(2));
				status.setCurrent(rs.getBoolean(3));
				status.setApartmentId(rs.getLong(4));
				status.setBillId(rs.getLong(5));
				status.setClientId(rs.getLong(6));
				list.add(status);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		}finally{
			try {
				ps.close();
			}catch (SQLException e) {
				logger.error(STATEMENT_ERROR_MESSAGE);
			}
			try{
				conn.close();
			} catch (SQLException e) {
				logger.error(CONNECTION_ERROR_MESSAGE);
			}
		}
		return list;
	}

	@Override
	public void setCurrent(long apartmentId, boolean current) 
			throws DAOException, ConnectionException {
		ProxyConnection conn = null;
		PreparedStatement ps = null;
		try{
			conn = ProxyConnectionPool.getInstance().getConnection();
			ps = conn.prepareStatement(SET_CURRENT);
			ps.setBoolean(1, current);
			ps.setLong(2, apartmentId);
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		}finally{
			try {
				ps.close();
			}catch (SQLException e) {
				logger.error(STATEMENT_ERROR_MESSAGE);
			}
			try{
				conn.close();
			} catch (SQLException e) {
				logger.error(CONNECTION_ERROR_MESSAGE);
			}
		}
	}
}
