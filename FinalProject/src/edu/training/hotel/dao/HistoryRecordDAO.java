package edu.training.hotel.dao;

import java.util.ArrayList;

import edu.training.hotel.entity.HistoryRecord;
import edu.training.hotel.exception.ConnectionException;
import edu.training.hotel.exception.DAOException;

public interface HistoryRecordDAO {

	ArrayList<HistoryRecord> selectHistory(long apartmentId) throws DAOException,
			ConnectionException;
	
	void createRecord(HistoryRecord record) throws DAOException,
			ConnectionException;
}
