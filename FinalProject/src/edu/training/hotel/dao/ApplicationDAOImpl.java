package edu.training.hotel.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import edu.training.hotel.connectionpool.ProxyConnection;
import edu.training.hotel.connectionpool.ProxyConnectionPool;
import edu.training.hotel.entity.Application;
import edu.training.hotel.exception.ConnectionException;
import edu.training.hotel.exception.DAOException;

public class ApplicationDAOImpl implements ApplicationDAO{

	private static final String SELECT_ALL = "select * from "
			+ "hotel.applications;";
	private static final String SELECT_NEW = "select * from "
			+ "hotel.applications where application_status = 'New';";
	private static final String SELECT_ALL_FOR_CLIENT = "select * from "
			+ "hotel.applications where client_id = ?;";
	private static final String FIND = "select * from hotel.applications "
			+ "where application_id = ?;";
	private static final String INSERT = "insert into hotel.applications"
			+ " (number_of_beds, number_of_people, arrival_date, "
			+ "number_of_days, apartment_type, client_id, application_date,"
			+ " application_status) values (?, ?, ?, ?, ?, ?, ?, ?);";
	private static final String UPDATE = "update hotel.applications"
			+ " set number_of_beds = ?, number_of_people = ?, "
			+ "arrival_date = ?, number_of_days = ?, apartment_type = ?, "
			+ "client_id = ?, application_date = ?, "
			+ "application_status = ? where application_id = ?;";
	private static final String DELETE = "delete from hotel.applications "
			+ "where application_id = ?;";
	private static final String STATEMENT_ERROR_MESSAGE = "������ ��� ��������"
			+ " ������� prepaired statement";
	private static final String CONNECTION_ERROR_MESSAGE = "������ ��� "
			+ "�������� ������� connection";
	
	static Logger logger = Logger.getLogger(ApplicationDAOImpl.class);
	
	public ApplicationDAOImpl() {

	}

	@Override
	public ArrayList<Application> selectApplications() throws DAOException, ConnectionException {
		ArrayList<Application> list = new ArrayList<Application>();
		ProxyConnection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try{
			conn = ProxyConnectionPool.getInstance().getConnection();
			ps = conn.prepareStatement(SELECT_ALL);
			rs = ps.executeQuery();
			while(rs.next()){
				Application application = new Application();
				application.setApplicationId(rs.getLong(1));
				application.setNumberOfBeds(rs.getInt(2));
				application.setNumberOfPeople(rs.getInt(3));
				application.setArrivalDate(rs.getDate(4));
				application.setNumberOfDays(rs.getInt(5));
				application.setAppartmentType(rs.getString(6));
				application.setClientId(rs.getLong(7));
				application.setApplicationDate(rs.getDate(8));
				application.setApplicationStatus(rs.getString(9));
				list.add(application);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		}finally{
			try {
				ps.close();
			}catch (SQLException e) {
				logger.error(STATEMENT_ERROR_MESSAGE);
			}
			try{
				conn.close();
			} catch (SQLException e) {
				logger.error(CONNECTION_ERROR_MESSAGE);
			}
		}
		return list;
	}
	
	@Override
	public ArrayList<Application> selectApplications(long clientId) throws DAOException, ConnectionException {
		ArrayList<Application> list = new ArrayList<Application>();
		ProxyConnection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try{ 
			conn = ProxyConnectionPool.getInstance().getConnection();
			ps = conn.prepareStatement(SELECT_ALL_FOR_CLIENT);
			ps.setLong(1, clientId);
			rs = ps.executeQuery();
			while(rs.next()){
				Application application = new Application();
				application.setApplicationId(rs.getLong(1));
				application.setNumberOfBeds(rs.getInt(2));
				application.setNumberOfPeople(rs.getInt(3));
				application.setArrivalDate(rs.getDate(4));
				application.setNumberOfDays(rs.getInt(5));
				application.setAppartmentType(rs.getString(6));
				application.setClientId(rs.getLong(7));
				application.setApplicationDate(rs.getDate(8));
				application.setApplicationStatus(rs.getString(9));
				list.add(application);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		}finally{
			try {
				ps.close();
			}catch (SQLException e) {
				logger.error(STATEMENT_ERROR_MESSAGE);
			}
			try{
				conn.close();
			} catch (SQLException e) {
				logger.error(CONNECTION_ERROR_MESSAGE);
			}
		}
		return list;
	}

	@Override
	public Application selectApplication(long id) throws DAOException, ConnectionException {
		ProxyConnection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Application application = new Application();
		try{ 
			conn = ProxyConnectionPool.getInstance().getConnection();
			ps = conn.prepareStatement(FIND);
			ps.setLong(1, id);
			rs = ps.executeQuery();
			while(rs.next()){
				application.setApplicationId(rs.getLong(1));
				application.setNumberOfBeds(rs.getInt(2));
				application.setNumberOfPeople(rs.getInt(3));
				application.setArrivalDate(rs.getDate(4));
				application.setNumberOfDays(rs.getInt(5));
				application.setAppartmentType(rs.getString(6));
				application.setClientId(rs.getLong(7));
				application.setApplicationDate(rs.getDate(8));
				application.setApplicationStatus(rs.getString(9));
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		}finally{
			try {
				ps.close();
			}catch (SQLException e) {
				logger.error(STATEMENT_ERROR_MESSAGE);
			}
			try{
				conn.close();
			} catch (SQLException e) {
				logger.error(CONNECTION_ERROR_MESSAGE);
			}
		}
		return application;
	}

	@Override
	public void createApplication(Application application) throws DAOException, ConnectionException {
		ProxyConnection conn = null;
		PreparedStatement ps = null;
		try{ 
			conn = ProxyConnectionPool.getInstance().getConnection();
			ps = conn.prepareStatement(INSERT);
			if(application.getNumberOfBeds()!= 0){
				ps.setInt(1, application.getNumberOfBeds());
			}else{
				ps.setNull(1, java.sql.Types.INTEGER);
			}
			ps.setInt(2, application.getNumberOfPeople());
			ps.setDate(3, new java.sql.Date(application
					.getArrivalDate().getTime()));
			ps.setInt(4, application.getNumberOfDays());
			ps.setString(5, application.getAppartmentType());
			ps.setLong(6, application.getClientId());
			ps.setDate(7, new java.sql.Date(application
					.getApplicationDate().getTime()));
			ps.setString(8, application.getApplicationStatus());
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		}finally{
			try {
				ps.close();
			}catch (SQLException e) {
				logger.error(STATEMENT_ERROR_MESSAGE);
			}
			try{
				conn.close();
			} catch (SQLException e) {
				logger.error(CONNECTION_ERROR_MESSAGE);
			}
		}
	}

	@Override
	public void updateApplication(long id, Application application) throws DAOException, ConnectionException {
		ProxyConnection conn = null;
		PreparedStatement ps = null;
		try{ 
			conn = ProxyConnectionPool.getInstance().getConnection();
			ps = conn.prepareStatement(UPDATE);
			ps.setInt(1, application.getNumberOfBeds());
			ps.setInt(2, application.getNumberOfPeople());
			ps.setDate(3, new java.sql.Date(application
					.getArrivalDate().getTime()));
			ps.setInt(4, application.getNumberOfDays());
			ps.setString(5, application.getAppartmentType());
			ps.setLong(6, application.getClientId());
			ps.setDate(7, new java.sql.Date(application
					.getApplicationDate().getTime()));
			ps.setString(8, application.getApplicationStatus());
			ps.setLong(9, id);
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		}finally{
			try {
				ps.close();
			}catch (SQLException e) {
				logger.error(STATEMENT_ERROR_MESSAGE);
			}
			try{
				conn.close();
			} catch (SQLException e) {
				logger.error(CONNECTION_ERROR_MESSAGE);
			}
		}
	}

	@Override
	public void deleteApplication(long id) throws DAOException, ConnectionException {
		ProxyConnection conn = null;
		PreparedStatement ps = null;
		try{ 
			conn = ProxyConnectionPool.getInstance().getConnection();
			ps = conn.prepareStatement(DELETE);
			ps.setLong(1, id);
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		}finally{
			try {
				ps.close();
			}catch (SQLException e) {
				logger.error(STATEMENT_ERROR_MESSAGE);
			}
			try{
				conn.close();
			} catch (SQLException e) {
				logger.error(CONNECTION_ERROR_MESSAGE);
			}
		}
	}

	@Override
	public ArrayList<Application> selectNewApplications() throws DAOException, ConnectionException {
		ArrayList<Application> list = new ArrayList<Application>();
		ProxyConnection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try{ 
			conn = ProxyConnectionPool.getInstance().getConnection();
			ps = conn.prepareStatement(SELECT_NEW);
			rs = ps.executeQuery();
			while(rs.next()){
				Application application = new Application();
				application.setApplicationId(rs.getLong(1));
				application.setNumberOfBeds(rs.getInt(2));
				application.setNumberOfPeople(rs.getInt(3));
				application.setArrivalDate(rs.getDate(4));
				application.setNumberOfDays(rs.getInt(5));
				application.setAppartmentType(rs.getString(6));
				application.setClientId(rs.getLong(7));
				application.setApplicationDate(rs.getDate(8));
				application.setApplicationStatus(rs.getString(9));
				list.add(application);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		}finally{
			try {
				ps.close();
			}catch (SQLException e) {
				logger.error(STATEMENT_ERROR_MESSAGE);
			}
			try{
				conn.close();
			} catch (SQLException e) {
				logger.error(CONNECTION_ERROR_MESSAGE);
			}
		}
		return list;
	}

	
}
