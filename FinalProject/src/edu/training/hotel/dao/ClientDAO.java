package edu.training.hotel.dao;

import java.util.ArrayList;

import edu.training.hotel.entity.Client;
import edu.training.hotel.exception.ConnectionException;
import edu.training.hotel.exception.DAOException;

public interface ClientDAO {

	ArrayList<Client> selectClients() throws DAOException, 
			ConnectionException;
	
	Client selectClient(long id) throws DAOException, ConnectionException;
	
	void updateClient(long id, Client client) 
			throws DAOException, ConnectionException;
	
	void createClient(Client client) throws DAOException, 
			ConnectionException;
	
	long authorize(Client client) throws DAOException, 
		ConnectionException;
}
