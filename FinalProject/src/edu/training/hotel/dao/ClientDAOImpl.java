package edu.training.hotel.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import edu.training.hotel.connectionpool.ProxyConnection;
import edu.training.hotel.connectionpool.ProxyConnectionPool;
import edu.training.hotel.entity.Client;
import edu.training.hotel.exception.ConnectionException;
import edu.training.hotel.exception.DAOException;

public class ClientDAOImpl implements ClientDAO{

	private static final String SELECT_ALL = "select * from hotel.clients;";
	private static final String FIND = "select * from hotel.clients "
			+ "where client_id = ?;";
	private static final String UPDATE = "update hotel.clients set name = ?,"
			+ " surname = ?, age = ?, gender = ?, country = ?, password = ?,"
			+ " email = ? where client_id = ?;";
	private static final String INSERT = "insert into hotel.clients (name,"
			+ " surname, age, gender, country, password, email) "
			+ "values (?, ?, ?, ?, ?, ?, ?);";
	private static final String AUTHORIZE = "select client_id from"
			+ " hotel.clients where name=? && surname=? && password = ?; ";
	private static final String STATEMENT_ERROR_MESSAGE = "������ ��� ��������"
			+ " ������� prepaired statement";
	private static final String CONNECTION_ERROR_MESSAGE = "������ ��� "
			+ "�������� ������� connection";
	
	static Logger logger = Logger.getLogger(ClientDAOImpl.class);
	
	public ClientDAOImpl() {
		
	}

	@Override
	public ArrayList<Client> selectClients() throws DAOException,
			ConnectionException {
		ArrayList<Client> list = new ArrayList<Client>();
		ProxyConnection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try{ 
			conn = ProxyConnectionPool.getInstance().getConnection();
			ps = conn.prepareStatement(SELECT_ALL);
			rs = ps.executeQuery();
			while(rs.next()){
				Client client = new Client();
				client.setClientId(rs.getLong(1));
				client.setName(rs.getString(2));
				client.setSurname(rs.getString(3));
				client.setAge(rs.getInt(4));
				client.setGender(rs.getString(5));
				client.setCountry(rs.getString(6));
				client.setPassword(rs.getString(7));
				client.seteMail(rs.getString(8));
				list.add(client);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		}finally{
			try {
				ps.close();
			}catch (SQLException e) {
				logger.error(STATEMENT_ERROR_MESSAGE);
			}
			try{
				conn.close();
			} catch (SQLException e) {
				logger.error(CONNECTION_ERROR_MESSAGE);
			}
		}
		return list;
	}

	@Override
	public Client selectClient(long id) throws DAOException,
			ConnectionException {
		Client client = new Client();
		ProxyConnection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try{ 
			conn = ProxyConnectionPool.getInstance().getConnection();
			ps = conn.prepareStatement(FIND);
			ps.setLong(1, id);
			rs = ps.executeQuery();
			while(rs.next()){
				
				client.setClientId(rs.getLong(1));
				client.setName(rs.getString(2));
				client.setSurname(rs.getString(3));
				client.setAge(rs.getInt(4));
				client.setGender(rs.getString(5));
				client.setCountry(rs.getString(6));
				client.setPassword(rs.getString(7));
				client.seteMail(rs.getString(8));
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		}finally{
			try {
				ps.close();
			}catch (SQLException e) {
				logger.error(STATEMENT_ERROR_MESSAGE);
			}
			try{
				conn.close();
			} catch (SQLException e) {
				logger.error(CONNECTION_ERROR_MESSAGE);
			}
		}
		return client;
	}

	@Override
	public void updateClient(long id, Client client) throws DAOException,
			ConnectionException {
		ProxyConnection conn = null;
		PreparedStatement ps = null;
		try{ 
			conn = ProxyConnectionPool.getInstance().getConnection();
			ps = conn.prepareStatement(UPDATE);
			ps.setString(1, client.getName());
			ps.setString(2, client.getSurname());
			ps.setInt(3, client.getAge());
			ps.setString(4, client.getGender());
			ps.setString(5, client.getCountry());
			ps.setString(6, client.getPassword());
			ps.setString(7, client.geteMail());
			ps.setLong(8, id);
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		}finally{
			try {
				ps.close();
			}catch (SQLException e) {
				logger.error(STATEMENT_ERROR_MESSAGE);
			}
			try{
				conn.close();
			} catch (SQLException e) {
				logger.error(CONNECTION_ERROR_MESSAGE);
			}
		}
	}

	@Override
	public void createClient(Client client) throws DAOException,
			ConnectionException {
		ProxyConnection conn = null;
		PreparedStatement ps = null;
		try{ 
			conn = ProxyConnectionPool.getInstance().getConnection();
			ps = conn.prepareStatement(INSERT);
			ps.setString(1, client.getName());
			ps.setString(2, client.getSurname());
			ps.setInt(3, client.getAge());
			ps.setString(4, client.getGender());
			ps.setString(5, client.getCountry());
			ps.setString(6, client.getPassword());
			ps.setString(7, client.geteMail());
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		}finally{
			try {
				ps.close();
			}catch (SQLException e) {
				logger.error(STATEMENT_ERROR_MESSAGE);
			}
			try{
				conn.close();
			} catch (SQLException e) {
				logger.error(CONNECTION_ERROR_MESSAGE);
			}
		}
	}

	@Override
	public long authorize(Client client) throws DAOException,
			ConnectionException {
		ProxyConnection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		long id = 0;
		try{ 
			conn = ProxyConnectionPool.getInstance().getConnection();
			ps = conn.prepareStatement(AUTHORIZE);
			ps.setString(1, client.getName());
			ps.setString(2, client.getSurname());
			ps.setString(3, client.getPassword());
			rs = ps.executeQuery();
			while(rs.next()){
				id = rs.getLong(1);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		}finally{
			try {
				ps.close();
			}catch (SQLException e) {
				logger.error(STATEMENT_ERROR_MESSAGE);
			}
			try{
				conn.close();
			} catch (SQLException e) {
				logger.error(CONNECTION_ERROR_MESSAGE);
			}
		}	
		return id;
	}


}
