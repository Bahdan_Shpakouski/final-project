package edu.training.hotel.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import edu.training.hotel.connectionpool.ProxyConnection;
import edu.training.hotel.connectionpool.ProxyConnectionPool;
import edu.training.hotel.entity.Administrator;
import edu.training.hotel.exception.ConnectionException;
import edu.training.hotel.exception.DAOException;

public class AdministratorDAOImpl implements AdministratorDAO {

	private static final String AUTHORIZE = "select administrator_id from"
			+ " hotel.administrators where name=? "
			+ "&& surname=? && password = ?; ";
	private static final String STATEMENT_ERROR_MESSAGE = "������ ��� ��������"
			+ " ������� prepaired statement";
	private static final String CONNECTION_ERROR_MESSAGE = "������ ��� "
			+ "�������� ������� connection";
	
	static Logger logger = Logger.getLogger(AdministratorDAOImpl.class);
	
	public AdministratorDAOImpl() {
		
	}

	@Override
	public boolean authorize(Administrator administrator) 
			throws DAOException, ConnectionException {
		ProxyConnection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		boolean found = false;
		try{ 
			conn = ProxyConnectionPool.getInstance().getConnection();
			ps = conn.prepareStatement(AUTHORIZE);
			ps.setString(1, administrator.getName());
			ps.setString(2, administrator.getSurname());
			ps.setString(3, administrator.getPassword());
			rs = ps.executeQuery();
			while(rs.next()){
				long id = rs.getLong(1);
				if(id != 0){
					found = true;
				}
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		}finally{
			try {
				ps.close();
			}catch (SQLException e) {
				logger.error(STATEMENT_ERROR_MESSAGE);
			}
			try{
				conn.close();
			} catch (SQLException e) {
				logger.error(CONNECTION_ERROR_MESSAGE);
			}
		}	
		return found;
	}



}
