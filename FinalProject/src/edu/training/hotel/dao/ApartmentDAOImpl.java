package edu.training.hotel.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import edu.training.hotel.connectionpool.ProxyConnection;
import edu.training.hotel.connectionpool.ProxyConnectionPool;
import edu.training.hotel.entity.Apartment;
import edu.training.hotel.exception.ConnectionException;
import edu.training.hotel.exception.DAOException;

public class ApartmentDAOImpl implements ApartmentDAO{

	private static final String SELECT_ALL = "select apartments.* , has_tv,"
			+ " has_fridge, has_balcon, has_air_conditioner"
			+ " from hotel.apartments inner join hotel.additional_information"
			+ " on apartments.additional_information_id = "
			+ "additional_information.additional_information_id;";
	private static final String FIND = "select apartments.* , has_tv,"
			+ " has_fridge, has_balcon, has_air_conditioner"
			+ " from hotel.apartments inner join hotel.additional_information"
			+ " on apartments.additional_information_id = "
			+ "additional_information.additional_information_id "
			+ "where apartment_id = ?;";
	private static final String UPDATE = "update hotel.additional_information "
			+ "set has_tv = ?, has_fridge = ?,"
			+ " has_air_conditioner = ? where additional_information_id = ?";
	private static final String STATEMENT_ERROR_MESSAGE = "������ ��� ��������"
			+ " ������� prepaired statement";
	private static final String CONNECTION_ERROR_MESSAGE = "������ ��� "
			+ "�������� ������� connection";
	
	static Logger logger = Logger.getLogger(ApartmentDAOImpl.class);
	
	public ApartmentDAOImpl() {
		
	}

	@Override
	public ArrayList<Apartment> selectApartments() throws DAOException,
			ConnectionException {
		ArrayList<Apartment> list = new ArrayList<Apartment>();
		PreparedStatement ps = null;
		ProxyConnection conn = null;
		ResultSet rs = null;
		try{
			conn = ProxyConnectionPool.getInstance().getConnection();
			ps = conn.prepareStatement(SELECT_ALL);
			rs = ps.executeQuery();
			while(rs.next()){
				Apartment apartment = new Apartment();
				apartment.setApartmentId(rs.getLong(1));
				apartment.setApartmentNumber(rs.getInt(2));
				apartment.setFloor(rs.getInt(3));
				apartment.setNumberOfRooms(rs.getInt(4));
				apartment.setNumberOfBeds(rs.getInt(5));
				apartment.setNumberOfPlaces(rs.getInt(6));
				apartment.setApartmentType(rs.getString(7));
				apartment.setCost(rs.getInt(8));
				apartment.setAdditionalInformation(rs.getLong(9),
						rs.getBoolean(10), rs.getBoolean(11),
						rs.getBoolean(12), rs.getBoolean(13));
				list.add(apartment);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		}finally{
			try {
				ps.close();
			}catch (SQLException e) {
				logger.error(STATEMENT_ERROR_MESSAGE);
			}
			try{
				conn.close();
			} catch (SQLException e) {
				logger.error(CONNECTION_ERROR_MESSAGE);
			}
		}
		return list;
	}

	@Override
	public Apartment selectApartment(long id) throws DAOException,
			ConnectionException {
		Apartment apartment = new Apartment();
		ProxyConnection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try{
			conn = ProxyConnectionPool.getInstance().getConnection();
			ps = conn.prepareStatement(FIND);
			ps.setLong(1, id);
			rs = ps.executeQuery();
			while(rs.next()){
				apartment.setApartmentId(rs.getLong(1));
				apartment.setApartmentNumber(rs.getInt(2));
				apartment.setFloor(rs.getInt(3));
				apartment.setNumberOfRooms(rs.getInt(4));
				apartment.setNumberOfBeds(rs.getInt(5));
				apartment.setNumberOfPlaces(rs.getInt(6));
				apartment.setApartmentType(rs.getString(7));
				apartment.setCost(rs.getInt(8));
				apartment.setAdditionalInformation(rs.getLong(9),
						rs.getBoolean(10), rs.getBoolean(11),
						rs.getBoolean(12), rs.getBoolean(13));
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		}finally{
			try {
				ps.close();
			}catch (SQLException e) {
				logger.error(STATEMENT_ERROR_MESSAGE);
			}
			try{
				conn.close();
			} catch (SQLException e) {
				logger.error(CONNECTION_ERROR_MESSAGE);
			}
		}
		return apartment;
	}

	@Override
	public void updateAdditionalInformation(long id, boolean hasTV,
			boolean hasFridge, boolean hasAirConditioner) 
				throws DAOException, ConnectionException{
		ProxyConnection conn = null;
		PreparedStatement ps = null;
		try{
			conn = ProxyConnectionPool.getInstance().getConnection();
			ps = conn.prepareStatement(UPDATE);
			ps.setBoolean(1, hasTV);
			ps.setBoolean(2, hasFridge);
			ps.setBoolean(3, hasAirConditioner);
			ps.setLong(4, id);
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		}finally{
			try {
				ps.close();
			}catch (SQLException e) {
				logger.error(STATEMENT_ERROR_MESSAGE);
			}
			try{
				conn.close();
			} catch (SQLException e) {
				logger.error(CONNECTION_ERROR_MESSAGE);
			}
		}
	}

}
