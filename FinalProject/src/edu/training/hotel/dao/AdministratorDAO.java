package edu.training.hotel.dao;

import edu.training.hotel.entity.Administrator;
import edu.training.hotel.exception.ConnectionException;
import edu.training.hotel.exception.DAOException;

public interface AdministratorDAO {

	boolean authorize(Administrator administrator) throws DAOException, 
		ConnectionException;
}
