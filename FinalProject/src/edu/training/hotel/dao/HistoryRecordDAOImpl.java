package edu.training.hotel.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import edu.training.hotel.connectionpool.ProxyConnection;
import edu.training.hotel.connectionpool.ProxyConnectionPool;
import edu.training.hotel.entity.HistoryRecord;
import edu.training.hotel.exception.ConnectionException;
import edu.training.hotel.exception.DAOException;

public class HistoryRecordDAOImpl implements HistoryRecordDAO {

	private static final String SELECT_ALL = "select * from "
			+ "hotel.hotel_history where apartment_id = ?;";
	private static final String INSERT = "insert into hotel_history"
			+ " (moving_in_date, moving_out_date, client_id, apartment_id) "
			+ "values (?, ?, ?, ?);";
	private static final String STATEMENT_ERROR_MESSAGE = "������ ��� ��������"
			+ " ������� prepaired statement";
	private static final String CONNECTION_ERROR_MESSAGE = "������ ��� "
			+ "�������� ������� connection";
	
	static Logger logger = Logger.getLogger(HistoryRecordDAOImpl.class);
	
	public HistoryRecordDAOImpl() {
		
	}

	@Override
	public ArrayList<HistoryRecord> selectHistory(long apartmentId) throws DAOException, ConnectionException {
		ArrayList<HistoryRecord> list = new ArrayList<HistoryRecord>();
		ProxyConnection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try{ 
			conn = ProxyConnectionPool.getInstance().getConnection();
			ps = conn.prepareStatement(SELECT_ALL);
			ps.setLong(1, apartmentId);
			rs = ps.executeQuery();
			while(rs.next()){
				HistoryRecord record = new HistoryRecord();
				record.setRecordId(rs.getLong(1));
				record.setMovingInDate(rs.getDate(2));
				record.setMovingOutDate(rs.getDate(3));
				record.setClientId(rs.getInt(4));
				record.setApartmentId(rs.getInt(5));
				list.add(record);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		}finally{
			try {
				ps.close();
			}catch (SQLException e) {
				logger.error(STATEMENT_ERROR_MESSAGE);
			}
			try{
				conn.close();
			} catch (SQLException e) {
				logger.error(CONNECTION_ERROR_MESSAGE);
			}
		}
		return list;
	}

	@Override
	public void createRecord(HistoryRecord record) throws DAOException, ConnectionException {
		ProxyConnection conn = null;
		PreparedStatement ps = null;
		try{ 
			conn = ProxyConnectionPool.getInstance().getConnection();
			ps = conn.prepareStatement(INSERT);
			ps.setDate(1, new java.sql.Date(record.getMovingInDate().getTime()));
			ps.setDate(2, new java.sql.Date(record.getMovingOutDate().getTime()));
			ps.setLong(3, record.getClientId());
			ps.setLong(4, record.getApartmentId());
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		}finally{
			try {
				ps.close();
			}catch (SQLException e) {
				logger.error(STATEMENT_ERROR_MESSAGE);
			}
			try{
				conn.close();
			} catch (SQLException e) {
				logger.error(CONNECTION_ERROR_MESSAGE);
			}
		}
	}
}
