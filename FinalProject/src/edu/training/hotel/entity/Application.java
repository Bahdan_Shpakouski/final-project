package edu.training.hotel.entity;

import java.util.Date;

public class Application {

	private long applicationId;
	private int numberOfBeds;
	private int numberOfPeople;
	private Date arrivalDate;
	private int numberOfDays;
	private String appartmentType;
	private long clientId;
	private Date applicationDate;
	private String applicationStatus;
	
	public Application() {
		
	}

	public long getApplicationId() {
		return applicationId;
	}

	public void setApplicationId(long applicationId) {
		this.applicationId = applicationId;
	}

	public int getNumberOfBeds() {
		return numberOfBeds;
	}

	public void setNumberOfBeds(int numberOfBeds) {
		this.numberOfBeds = numberOfBeds;
	}

	public int getNumberOfPeople() {
		return numberOfPeople;
	}

	public void setNumberOfPeople(int numberOfPeople) {
		this.numberOfPeople = numberOfPeople;
	}

	public Date getArrivalDate() {
		return arrivalDate;
	}

	public void setArrivalDate(Date arrivalDate) {
		this.arrivalDate = arrivalDate;
	}

	public int getNumberOfDays() {
		return numberOfDays;
	}

	public void setNumberOfDays(int numberOfDays) {
		this.numberOfDays = numberOfDays;
	}

	public String getAppartmentType() {
		return appartmentType;
	}

	public void setAppartmentType(String appartmentType) {
		this.appartmentType = appartmentType;
	}

	public long getClientId() {
		return clientId;
	}

	public void setClientId(long clientId) {
		this.clientId = clientId;
	}

	public Date getApplicationDate() {
		return applicationDate;
	}

	public void setApplicationDate(Date applicationDate) {
		this.applicationDate = applicationDate;
	}

	public String getApplicationStatus() {
		return applicationStatus;
	}

	public void setApplicationStatus(String applicationStatus) {
		this.applicationStatus = applicationStatus;
	}

	@Override
	public String toString() {
		return "Application [applicationId=" + applicationId + ", numberOfBeds=" + numberOfBeds + ", numberOfPeople="
				+ numberOfPeople + ", arrivalDate=" + arrivalDate + ", numberOfDays=" + numberOfDays
				+ ", appartmentType=" + appartmentType + ", clientId=" + clientId + ", applicationDate="
				+ applicationDate + ", applicationStatus=" + applicationStatus + "]";
	}

}
