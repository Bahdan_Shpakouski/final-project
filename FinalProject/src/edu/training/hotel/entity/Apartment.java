package edu.training.hotel.entity;

public class Apartment {

	private long apartmentId;
	private int apartmentNumber;
	private int floor;
	private int numberOfRooms;
	private int numberOfBeds;
	private int numberOfPlaces;
	private String apartmentType;
	private int cost;
	private AdditionalInformation additionalInformation;
	
	public class AdditionalInformation{
		
		private long additionalInformationId;
		private boolean hasTV;
		private boolean hasFridge;
		private boolean hasBalcon;
		private boolean hasAirConditioner;
		
		public AdditionalInformation(long additionalInformationId, 
				boolean hasTV, boolean hasFridge, boolean hasBalcon,
				boolean hasAirConditioner) {
			this.additionalInformationId = additionalInformationId;
			this.hasTV = hasTV;
			this.hasFridge = hasFridge;
			this.hasBalcon = hasBalcon;
			this.hasAirConditioner = hasAirConditioner;
		}

		public long getAdditionalInformationId() {
			return additionalInformationId;
		}

		public void setAdditionalInformationId(long additionalInformationId) {
			this.additionalInformationId = additionalInformationId;
		}

		public boolean isHasTV() {
			return hasTV;
		}
		
		public void setHasTV(boolean hasTV) {
			this.hasTV = hasTV;
		}
		
		public boolean isHasFridge() {
			return hasFridge;
		}
		
		public void setHasFridge(boolean hasFridge) {
			this.hasFridge = hasFridge;
		}
		
		public boolean isHasBalcon() {
			return hasBalcon;
		}
		
		public void setHasBalcon(boolean hasBalcon) {
			this.hasBalcon = hasBalcon;
		}
		
		public boolean isHasAirConditioner() {
			return hasAirConditioner;
		}
		
		public void setHasAirConditioner(boolean hasAirConditioner) {
			this.hasAirConditioner = hasAirConditioner;
		}

		@Override
		public String toString() {
			return "AdditionalInformation [additionalInformationId=" 
					+ additionalInformationId + ", hasTV=" + hasTV
					+ ", hasFridge=" + hasFridge + ", hasBalcon=" 
					+ hasBalcon + ", hasAirConditioner="
					+ hasAirConditioner + "]";
		}
	}
	
	public Apartment() {
		
	}	

	public long getApartmentId() {
		return apartmentId;
	}
	
	public void setApartmentId(long apartmentId) {
		this.apartmentId = apartmentId;
	}
	
	public int getApartmentNumber() {
		return apartmentNumber;
	}
	
	public void setApartmentNumber(int apartmentNumber) {
		this.apartmentNumber = apartmentNumber;
	}
	
	public int getFloor() {
		return floor;
	}
	
	public void setFloor(int floor) {
		this.floor = floor;
	}
	
	public int getNumberOfRooms() {
		return numberOfRooms;
	}
	
	public void setNumberOfRooms(int numberOfRooms) {
		this.numberOfRooms = numberOfRooms;
	}
	
	public int getNumberOfBeds() {
		return numberOfBeds;
	}
	
	public void setNumberOfBeds(int numberOfBeds) {
		this.numberOfBeds = numberOfBeds;
	}
	
	public int getNumberOfPlaces() {
		return numberOfPlaces;
	}
	
	public void setNumberOfPlaces(int numberOfPlaces) {
		this.numberOfPlaces = numberOfPlaces;
	}
	
	public String getApartmentType() {
		return apartmentType;
	}
	
	public void setApartmentType(String apartmentType) {
		this.apartmentType = apartmentType;
	}
	
	public int getCost() {
		return cost;
	}
	
	public void setCost(int cost) {
		this.cost = cost;
	}
	
	public AdditionalInformation getAdditionalInformation() {
		return additionalInformation;
	}
	
	public void setAdditionalInformation(AdditionalInformation 
			additionalInformation) {
		this.additionalInformation = additionalInformation;
	}
	
	public void setAdditionalInformation(long id, boolean hasTV,
			boolean hasFridge, boolean hasBalcon,
			boolean hasAirConditioner) {
		this.additionalInformation = new AdditionalInformation(id, hasTV,
				hasFridge, hasBalcon, hasAirConditioner);
	}

	@Override
	public String toString() {
		return "Apartment [apartmentId=" + apartmentId + ", apartmentNumber=" + apartmentNumber + ", floor=" + floor
				+ ", numberOfRooms=" + numberOfRooms + ", numberOfBeds=" + numberOfBeds + ", numberOfPlaces="
				+ numberOfPlaces + ", apartmentType=" + apartmentType + ", cost=" + cost + ", "
				+ additionalInformation.toString() + "]";
	}

}
