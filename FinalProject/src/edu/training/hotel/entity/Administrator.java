package edu.training.hotel.entity;

public class Administrator {

	private long administratorId;
	private String name;
	private String surname;
	private String password;
	
	public Administrator() {
		
	}

	public long getAdministratorId() {
		return administratorId;
	}

	public void setAdministratorId(long administratorId) {
		this.administratorId = administratorId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return "Administrator [administratorId=" + administratorId 
				+ ", name=" + name + ", surname=" + surname
				+ ", password=" + password + "]";
	}

}
