package edu.training.hotel.entity;

public class ApartmentStatus {

	private long statusId;
	private String status;
	private boolean current;
	private long apartmentId;
	private long billId;
	private long clientId;
	
	public ApartmentStatus() {
		
	}

	public long getStatusId() {
		return statusId;
	}

	public void setStatusId(long statusId) {
		this.statusId = statusId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public boolean isCurrent() {
		return current;
	}

	public void setCurrent(boolean current) {
		this.current = current;
	}

	public long getApartmentId() {
		return apartmentId;
	}

	public void setApartmentId(long apartmentId) {
		this.apartmentId = apartmentId;
	}

	public long getBillId() {
		return billId;
	}

	public void setBillId(long billId) {
		this.billId = billId;
	}

	public long getClientId() {
		return clientId;
	}

	public void setClientId(long clientId) {
		this.clientId = clientId;
	}

	@Override
	public String toString() {
		return "ApartmentStatus [statusId=" + statusId + ", status=" + status 
				+ ", current=" + current + ", apartmentId=" + apartmentId 
				+ ", billId=" + billId + ", clientId=" + clientId + "]";
	}
}
