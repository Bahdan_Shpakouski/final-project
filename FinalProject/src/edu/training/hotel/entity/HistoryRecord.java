package edu.training.hotel.entity;

import java.util.Date;

public class HistoryRecord {

	private long recordId;
	private Date movingInDate;
	private Date movingOutDate;
	private long clientId;
	private long apartmentId;
	
	public HistoryRecord() {
		
	}

	public long getRecordId() {
		return recordId;
	}

	public void setRecordId(long recordId) {
		this.recordId = recordId;
	}

	public Date getMovingInDate() {
		return movingInDate;
	}

	public void setMovingInDate(Date movingInDate) {
		this.movingInDate = movingInDate;
	}

	public Date getMovingOutDate() {
		return movingOutDate;
	}

	public void setMovingOutDate(Date movingOutDate) {
		this.movingOutDate = movingOutDate;
	}

	public long getClientId() {
		return clientId;
	}

	public void setClientId(long clientId) {
		this.clientId = clientId;
	}

	public long getApartmentId() {
		return apartmentId;
	}

	public void setApartmentId(long apartmentId) {
		this.apartmentId = apartmentId;
	}

	@Override
	public String toString() {
		return "HistoryRecord [recordId=" + recordId + ", movingInDate=" 
				+ movingInDate + ", movingOutDate=" + movingOutDate 
				+ ", clientId=" + clientId 
				+ ", apartmentId=" + apartmentId + "]";
	}

}
