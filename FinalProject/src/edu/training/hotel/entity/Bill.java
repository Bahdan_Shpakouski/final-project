package edu.training.hotel.entity;

import java.util.Date;

public class Bill {

	private long billId;
	private int cost;
	private Date movingInDate;
	private Date movingOutDate;
	private String billStatus;
	private long clientId;
	private long apartmentId;
	
	public Bill() {
		
	}

	public long getBillId() {
		return billId;
	}

	public void setBillId(long billId) {
		this.billId = billId;
	}

	public int getCost() {
		return cost;
	}

	public void setCost(int cost) {
		this.cost = cost;
	}

	public Date getMovingInDate() {
		return movingInDate;
	}

	public void setMovingInDate(Date movingInDate) {
		this.movingInDate = movingInDate;
	}

	public Date getMovingOutDate() {
		return movingOutDate;
	}

	public void setMovingOutDate(Date movingOutDate) {
		this.movingOutDate = movingOutDate;
	}

	public String getBillStatus() {
		return billStatus;
	}

	public void setBillStatus(String billStatus) {
		this.billStatus = billStatus;
	}

	public long getClientId() {
		return clientId;
	}

	public void setClientId(long clientId) {
		this.clientId = clientId;
	}

	public long getApartmentId() {
		return apartmentId;
	}

	public void setApartmentId(long apartmentId) {
		this.apartmentId = apartmentId;
	}

	@Override
	public String toString() {
		return "Bill [billId=" + billId + ", cost=" + cost + ", movingInDate="
				+ movingInDate + ", movingOutDate=" + movingOutDate 
				+ ", billStatus=" + billStatus + ", clientId=" 
				+ clientId + ", apartmentId=" + apartmentId + "]";
	}

}
