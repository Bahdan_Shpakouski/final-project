package edu.training.hotel.command;

import javax.servlet.http.HttpServletRequest;

import edu.training.hotel.exception.CommandException;

public class ShowApplicationFormCommand implements HotelCommand {

	private static final String PAGE = "WEB-INF/pages/newApplication.jsp";
	private static final String CLIENT_ID = "clientId";
	
	public ShowApplicationFormCommand() {
		
	}

	@Override
	public String process(HttpServletRequest request) throws CommandException {
		long id = Long.parseLong(request.getParameter(CLIENT_ID));
		request.setAttribute(CLIENT_ID, id);
		return PAGE;
	}

}
