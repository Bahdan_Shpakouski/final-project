package edu.training.hotel.command;

import javax.servlet.http.HttpServletRequest;

import edu.training.hotel.exception.CommandException;

public class ShowClientFormCommand implements HotelCommand {

	private static final String PAGE = "WEB-INF/pages/newClient.jsp";
	
	public ShowClientFormCommand() {
		
	}

	@Override
	public String process(HttpServletRequest request) throws CommandException {
		return PAGE;
	}

}
