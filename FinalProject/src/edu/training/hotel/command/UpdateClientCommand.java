package edu.training.hotel.command;

import javax.servlet.http.HttpServletRequest;

import edu.training.hotel.dao.ClientDAO;
import edu.training.hotel.dao.ClientDAOImpl;
import edu.training.hotel.entity.Client;
import edu.training.hotel.exception.CommandException;
import edu.training.hotel.exception.ConnectionException;
import edu.training.hotel.exception.DAOException;

public class UpdateClientCommand implements HotelCommand {

	private static final String PAGE = "index.jsp";
	private static final String CLIENT_ID = "clientId";
	private static final String NAME = "name";
	private static final String SURNAME = "surname";
	private static final String AGE = "age";
	private static final String GENDER = "gender";
	private static final String COUNTRY = "country";
	private static final String EMAIL = "email";
	
	public UpdateClientCommand() {
		
	}

	@Override
	public String process(HttpServletRequest request) throws CommandException {
		ClientDAO clientDAO = new ClientDAOImpl();
		long id = Long.parseLong(request.getParameter(CLIENT_ID));
		try {
			Client client = clientDAO.selectClient(id);
			client.setName(request.getParameter(NAME));
			client.setSurname(request.getParameter(SURNAME));
			client.setAge(Integer.parseInt(request.getParameter(AGE)));
			client.setGender(request.getParameter(GENDER));
			client.setCountry(request.getParameter(COUNTRY));
			client.seteMail(request.getParameter(EMAIL));
			clientDAO.updateClient(id, client);
		} catch (DAOException | ConnectionException e) {
			throw new CommandException(e);
		}
		return PAGE;
	}

}
