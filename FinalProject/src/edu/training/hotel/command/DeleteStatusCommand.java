package edu.training.hotel.command;

import javax.servlet.http.HttpServletRequest;

import edu.training.hotel.dao.ApartmentStatusDAO;
import edu.training.hotel.dao.ApartmentStatusDAOImpl;
import edu.training.hotel.exception.CommandException;
import edu.training.hotel.exception.ConnectionException;
import edu.training.hotel.exception.DAOException;

public class DeleteStatusCommand implements HotelCommand {

	private static final String PAGE ="index.jsp";
	private static final String STATUS_ID = "statusId";
	
	public DeleteStatusCommand() {
		
	}

	@Override
	public String process(HttpServletRequest request) throws CommandException {
		long id = Long.parseLong(request.getParameter(STATUS_ID));
		ApartmentStatusDAO statusDAO = new ApartmentStatusDAOImpl();
		try {
			statusDAO.deleteStatus(id);
		} catch (DAOException | ConnectionException e) {
			throw new CommandException(e);
		}
		return PAGE;
	}

}
