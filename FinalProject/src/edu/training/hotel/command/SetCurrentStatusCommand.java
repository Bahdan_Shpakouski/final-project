package edu.training.hotel.command;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import edu.training.hotel.dao.ApartmentStatusDAO;
import edu.training.hotel.dao.ApartmentStatusDAOImpl;
import edu.training.hotel.dao.BillDAO;
import edu.training.hotel.dao.BillDAOImpl;
import edu.training.hotel.entity.ApartmentStatus;
import edu.training.hotel.entity.Bill;
import edu.training.hotel.exception.CommandException;
import edu.training.hotel.exception.ConnectionException;
import edu.training.hotel.exception.DAOException;

public class SetCurrentStatusCommand implements HotelCommand{

	private static final String PAGE = "WEB-INF/pages/statuses.jsp";
	private static final String STATUS_ID = "statusId";
	private static final String STATUS_OCCUPIED = "Occupied";
	private static final String STATUS_LIST = "list";
	private static final String MODE = "mode";
	private static final String BILLS = "bills";
	private static final String DATE = "today";
	
	public SetCurrentStatusCommand() {
		
	}

	@Override
	public String process(HttpServletRequest request) throws CommandException {
		long id = Long.parseLong(request.getParameter(STATUS_ID));
		ApartmentStatusDAO statusDAO = new ApartmentStatusDAOImpl();
		ArrayList<ApartmentStatus> statusList = null;
		ArrayList<Bill> bills = new ArrayList<Bill>();
		BillDAO billDAO = new BillDAOImpl();
		try {
			ApartmentStatus status = statusDAO.selectStatus(id);
			long apartmentId = status.getApartmentId();
			status.setCurrent(true);
			status.setStatus(STATUS_OCCUPIED);
			statusDAO.updateStatus(id, status);
			statusDAO.setCurrent(apartmentId, false);
			statusList = statusDAO.selectCurrentStatuses();
			for( int i = 0; i <statusList.size() ; i++){
				long billId = statusList.get(i).getBillId();
				bills.add(billDAO.selectBill(billId));
			}
			Calendar c = Calendar.getInstance();
			c.set(Calendar.HOUR_OF_DAY, 0);
		    c.set(Calendar.MINUTE, 0);
		    c.set(Calendar.SECOND, 0);
		    c.set(Calendar.MILLISECOND, 0);
		    Date date = c.getTime();
			request.setAttribute(MODE, 0);
			request.setAttribute(BILLS, bills);
			request.setAttribute(DATE, date);
			request.setAttribute(STATUS_LIST, statusList);
		} catch (DAOException | ConnectionException e) {
			throw new CommandException(e);
		}
		return PAGE;
	}

}
