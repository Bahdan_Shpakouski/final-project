package edu.training.hotel.command;

import javax.servlet.http.HttpServletRequest;

import edu.training.hotel.dao.ClientDAO;
import edu.training.hotel.dao.ClientDAOImpl;
import edu.training.hotel.entity.Client;
import edu.training.hotel.exception.CommandException;
import edu.training.hotel.exception.ConnectionException;
import edu.training.hotel.exception.DAOException;

public class ShowUpdateClientFormCommand implements HotelCommand {

	private static final String PAGE = "WEB-INF/pages/updateClient.jsp";
	private static final String CLIENT_ID = "clientId";
	private static final String CLIENT = "client";
	
	public ShowUpdateClientFormCommand() {
		
	}

	@Override
	public String process(HttpServletRequest request) throws CommandException {
		long id = Long.parseLong(request.getParameter(CLIENT_ID));
		ClientDAO clientDAO = new ClientDAOImpl();
		try {
			Client client = clientDAO.selectClient(id);
			request.setAttribute(CLIENT, client);
		} catch (DAOException | ConnectionException e) {
			throw new CommandException(e);
		}
		return PAGE;
	}

}
