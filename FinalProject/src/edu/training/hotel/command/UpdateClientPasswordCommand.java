package edu.training.hotel.command;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.servlet.http.HttpServletRequest;

import edu.training.hotel.dao.ClientDAO;
import edu.training.hotel.dao.ClientDAOImpl;
import edu.training.hotel.entity.Client;
import edu.training.hotel.exception.CommandException;
import edu.training.hotel.exception.ConnectionException;
import edu.training.hotel.exception.DAOException;

public class UpdateClientPasswordCommand implements HotelCommand {

	private static final String PAGE = "index.jsp";
	private static final String CLIENT_ID = "clientId";
	private static final String PASSWORD = "password";
	private static final String ENCODING = "UTF-8";
	private static final String ENCRYPTION = "MD5";
	
	public UpdateClientPasswordCommand() {
		
	}

	@Override
	public String process(HttpServletRequest request) throws CommandException {
		ClientDAO clientDAO = new ClientDAOImpl();
		long id = Long.parseLong(request.getParameter(CLIENT_ID));
		try {
			Client client = clientDAO.selectClient(id);
			String password = request.getParameter(PASSWORD);
			String encryptedPassword;
			try {
				MessageDigest md = MessageDigest.getInstance(ENCRYPTION);
				md.update(password.getBytes(ENCODING));
				byte[] digest = md.digest();
				StringBuffer sb = new StringBuffer();
				for (byte b : digest) {
					sb.append(String.format("%02x", b & 0xff));
				}
				encryptedPassword = sb.toString();
			} catch (NoSuchAlgorithmException | UnsupportedEncodingException e1) {
				throw new CommandException("���������� �� �������");
			}
			client.setPassword(encryptedPassword);
			clientDAO.updateClient(id, client);
		} catch (DAOException | ConnectionException e) {
			throw new CommandException(e);
		}
		return PAGE;
	}

}
