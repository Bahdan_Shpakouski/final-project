package edu.training.hotel.command;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import edu.training.hotel.dao.BillDAO;
import edu.training.hotel.dao.BillDAOImpl;
import edu.training.hotel.entity.Bill;
import edu.training.hotel.exception.CommandException;
import edu.training.hotel.exception.ConnectionException;
import edu.training.hotel.exception.DAOException;

public class ShowBillsCommand implements HotelCommand {

	private static final String PAGE = "WEB-INF/pages/bills.jsp";
	private static final String NEW_ONLY = "newOnly";
	private static final String BILL_LIST = "list";
	
	public ShowBillsCommand() {
		
	}

	@Override
	public String process(HttpServletRequest request) throws CommandException {
		BillDAO billDAO = new BillDAOImpl();
		ArrayList<Bill> list;
		boolean newOnly = Boolean.parseBoolean(request
				.getParameter(NEW_ONLY));
		try {
			if(newOnly){
				list = billDAO.selectNewBills();
				request.setAttribute(NEW_ONLY, true);
			}else{
				list = billDAO.selectBills();
				request.setAttribute(NEW_ONLY, false);
			}
			request.setAttribute(BILL_LIST, list);
		} catch (DAOException | ConnectionException e) {
			throw new CommandException(e);
		}
		return PAGE;
	}

}
