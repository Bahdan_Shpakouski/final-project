package edu.training.hotel.command;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import edu.training.hotel.dao.ApplicationDAO;
import edu.training.hotel.dao.ApplicationDAOImpl;
import edu.training.hotel.entity.Application;
import edu.training.hotel.exception.CommandException;
import edu.training.hotel.exception.ConnectionException;
import edu.training.hotel.exception.DAOException;

public class DeclineApplicationCommand implements HotelCommand {

	private static final String PAGE = "WEB-INF/pages/applications.jsp";
	private static final String STATUS = "Declined";
	private static final String APPLICATION_ID = "applicationId";
	private static final String NEW_ONLY = "newOnly";
	private static final String APPLICATION_LIST = "list";
	
	public DeclineApplicationCommand() {
		
	}

	@Override
	public String process(HttpServletRequest request) throws CommandException {
		ApplicationDAO applicationDAO = new ApplicationDAOImpl();
		long id = Long.parseLong(request.getParameter(APPLICATION_ID));
		try {
			ArrayList<Application> list = applicationDAO.selectNewApplications();
			request.setAttribute(NEW_ONLY, true);
			request.setAttribute(APPLICATION_LIST, list);
			Application application = applicationDAO.selectApplication(id);
			application.setApplicationStatus(STATUS);
			applicationDAO.updateApplication(id, application);
		} catch (DAOException | ConnectionException e) {
			throw new CommandException(e);
		}
		return PAGE;
	}

}
