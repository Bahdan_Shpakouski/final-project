package edu.training.hotel.command;

import javax.servlet.http.HttpServletRequest;

import edu.training.hotel.exception.CommandException;

public class HotelCommandFactory {
	
	private enum CommandNames{ 
		SHOW_APARTMENTS, SHOW_APARTMENT, SHOW_CLIENTS, SHOW_APPLICATIONS,
		SHOW_CLIENT, SERVE_APPLICATION, SHOW_BILLS, SHOW_APPLICATION_FORM,
		CREATE_APPLICATION, SHOW_CLIENT_FORM, CREATE_CLIENT, CREATE_BILL,
		CONFIRM_BILL, DECLINE_APPLICATION, PAY_BILL, DECLINE_BILL,
		UPDATE_APPLICATION, DELETE_APPLICATION, SHOW_UPDATE_APPLICATION_FORM,
		SHOW_UPDATE_CLIENT_FORM, UPDATE_CLIENT, UPDATE_APARTMENT, 
		SHOW_STATUSES, DELETE_STATUS, SET_CURRENT_STATUS, SET_EXPIRED_STATUS,
		SHOW_HISTORY, SHOW_AUTHORIZATION_FORM, LOG_IN, LOG_OUT, 
		UPDATE_CLIENT_PASSWORD, CHANGE_LOCALE
	}
	
	private static final String COMMAND_PARAMETR = "command";
	
	public HotelCommandFactory() {
		
	}

	public HotelCommand getCommand(HttpServletRequest request) 
			throws CommandException{
		HotelCommand command = null;
		if(request.getParameter(COMMAND_PARAMETR) == null){
			throw new CommandException("Команда отсутствует");
		}
		CommandNames name = CommandNames.valueOf(request
				.getParameter(COMMAND_PARAMETR).toUpperCase());
		switch(name){
		case SHOW_APARTMENTS:
			command = new ShowApartmentsCommand();
			break;
		case SHOW_APARTMENT:
			command = new ShowApartmentCommand();
			break;
		case SHOW_CLIENTS:
			command = new ShowClientsCommand();
			break;
		case SHOW_CLIENT:
			command = new ShowClientCommand();
			break;
		case SHOW_APPLICATIONS:
			command = new ShowApplicationsCommand();
			break;
		case SERVE_APPLICATION:
			command = new ServeApplicationCommand();
			break;
		case SHOW_BILLS:
			command = new ShowBillsCommand();
			break;
		case SHOW_APPLICATION_FORM:
			command = new ShowApplicationFormCommand();
			break;
		case CREATE_APPLICATION:
			command = new CreateApplicationCommand();
			break;
		case SHOW_CLIENT_FORM:
			command = new ShowClientFormCommand();
			break;
		case CREATE_CLIENT:
			command = new CreateClientCommand();
			break;
		case CREATE_BILL:
			command = new CreateBillCommand();
			break;
		case CONFIRM_BILL:
			command = new ConfirmBillCommand();
			break;
		case DECLINE_APPLICATION:
			command = new DeclineApplicationCommand();
			break;
		case PAY_BILL:
			command = new PayBillCommand();
			break;
		case DECLINE_BILL:
			command = new DeclineBillCommand();
			break;
		case UPDATE_APPLICATION:
			command = new UpdateApplicationCommand();
			break;
		case DELETE_APPLICATION:
			command = new DeleteApplicationCommand();
			break;
		case SHOW_UPDATE_APPLICATION_FORM:
			command = new ShowUpdateApplicationFormCommand();
			break;
		case SHOW_UPDATE_CLIENT_FORM:
			command = new ShowUpdateClientFormCommand();
			break;
		case UPDATE_CLIENT:
			command = new UpdateClientCommand();
			break;
		case UPDATE_APARTMENT:
			command = new UpdateApartmentCommand();
			break;
		case SHOW_STATUSES:
			command = new ShowStatusesCommand();
			break;
		case DELETE_STATUS:
			command = new DeleteStatusCommand();
			break;
		case SET_CURRENT_STATUS:
			command = new SetCurrentStatusCommand();
			break;
		case SET_EXPIRED_STATUS:
			command = new SetExpiredStatusCommand();
			break;
		case SHOW_HISTORY:
			command = new ShowHistoryCommand();
			break;
		case SHOW_AUTHORIZATION_FORM:
			command = new ShowAuthorizationFormCommand();
			break;
		case LOG_IN:
			command = new LogInCommand();
			break;
		case LOG_OUT:
			command = new LogOutCommand();
			break;
		case UPDATE_CLIENT_PASSWORD:
			command = new UpdateClientPasswordCommand();
			break;
		case CHANGE_LOCALE:
			command = new ChangeLocaleCommand();
			break;
		}
		return command;
	}
}
