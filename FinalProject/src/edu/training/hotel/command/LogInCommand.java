package edu.training.hotel.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import edu.training.hotel.dao.AdministratorDAO;
import edu.training.hotel.dao.AdministratorDAOImpl;
import edu.training.hotel.dao.ClientDAO;
import edu.training.hotel.dao.ClientDAOImpl;
import edu.training.hotel.entity.Administrator;
import edu.training.hotel.entity.Client;
import edu.training.hotel.exception.CommandException;
import edu.training.hotel.exception.ConnectionException;
import edu.training.hotel.exception.DAOException;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class LogInCommand implements HotelCommand {

	private static final String PAGE = "index.jsp";
	private static final String AUTHORIZATION_PAGE = "WEB-INF/"
			+ "pages/authorization.jsp";
	private static final String NAME = "name";
	private static final String SURNAME = "surname";
	private static final String PASSWORD = "password";
	private static final String AUTHORIZATION_FAILED = "authoriztion_failed";
	private static final String ROLE = "role";
	private static final String ROLE_CLIENT = "client";
	private static final String ROLE_ADMINISTRATOR = "administrator";
	private static final String CLIENT_ID = "clientId";
	private static final String ENCODING = "UTF-8";
	private static final String ENCRYPTION = "MD5";
	
	public LogInCommand() {
		
	}

	@Override
	public String process(HttpServletRequest request) throws CommandException {
		Client client = new Client();
		ClientDAO clientDAO = new ClientDAOImpl();
		AdministratorDAO administratorDAO = new AdministratorDAOImpl();
		Administrator administrator = new Administrator();
		String name = request.getParameter(NAME);
		String surname = request.getParameter(SURNAME);
		String password = request.getParameter(PASSWORD);
		String encryptedPassword;
		try {
			MessageDigest md = MessageDigest.getInstance(ENCRYPTION);
			md.update(password.getBytes(ENCODING));
			byte[] digest = md.digest();
			StringBuffer sb = new StringBuffer();
			for (byte b : digest) {
				sb.append(String.format("%02x", b & 0xff));
			}
			encryptedPassword = sb.toString();
		} catch (NoSuchAlgorithmException | UnsupportedEncodingException e1) {
			throw new CommandException("���������� �� �������");
		}
		client.setName(name);
		client.setSurname(surname);
		client.setPassword(encryptedPassword);
		administrator.setName(name);
		administrator.setSurname(surname);
		administrator.setPassword(encryptedPassword);
		try {
			long clientId = clientDAO.authorize(client);
			if(clientId != 0){
				HttpSession session = request.getSession(true);
				session.setAttribute(ROLE, ROLE_CLIENT);
				session.setAttribute(CLIENT_ID, clientId);			
			}else{
				boolean found = administratorDAO.authorize(administrator);
				if(found){
					HttpSession session = request.getSession(true);
					session.setAttribute(ROLE, ROLE_ADMINISTRATOR);
				}else{
					request.setAttribute(AUTHORIZATION_FAILED, "true");
					return AUTHORIZATION_PAGE;
				}
			}
		} catch (DAOException | ConnectionException e) {
			throw new CommandException(e);
		}
		request.getSession();
		return PAGE;
	}

}
