package edu.training.hotel.command;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import edu.training.hotel.dao.ApplicationDAO;
import edu.training.hotel.dao.ApplicationDAOImpl;
import edu.training.hotel.entity.Application;
import edu.training.hotel.exception.CommandException;
import edu.training.hotel.exception.ConnectionException;
import edu.training.hotel.exception.DAOException;

public class UpdateApplicationCommand implements HotelCommand {

	private static final String PAGE = "index.jsp";
	private static final String APPLICATION_ID = "applicationId";
	private static final String NUMBER_OF_BEDS = "number_of_beds";
	private static final String NUMBER_OF_PEOPLE = "number_of_people";
	private static final String ARRIVAL_DATE = "date";
	private static final String NUMBER_OF_DAYS = "number_of_days";
	private static final String SELECT_CONTROL = "select_control";
	
	
	public UpdateApplicationCommand() {
		
	}

	@Override
	public String process(HttpServletRequest request) throws CommandException {
		ApplicationDAO applicationDAO = new ApplicationDAOImpl();
		long applicationId = Long.parseLong(request.getParameter(APPLICATION_ID));
		try{
			Application application = applicationDAO.selectApplication(applicationId);
			String numberOfBeds;
			if(!(numberOfBeds = request.getParameter(NUMBER_OF_BEDS)).isEmpty()){
				application.setNumberOfBeds(Integer.parseInt(numberOfBeds));
			}
			int numberOfPeople = Integer.parseInt(request
					.getParameter(NUMBER_OF_PEOPLE));
			application.setNumberOfPeople(numberOfPeople);
			String arrivalDate = request.getParameter(ARRIVAL_DATE);
			DateFormat format = new SimpleDateFormat("dd-MM-yyyy");
			Date date = format.parse(arrivalDate);
			application.setArrivalDate(date);
			int numberOfDays = Integer.parseInt(request.getParameter(NUMBER_OF_DAYS));
			application.setNumberOfDays(numberOfDays);
			String type = request.getParameter(SELECT_CONTROL);
			application.setAppartmentType(type);
			applicationDAO.updateApplication(applicationId, application);
		} catch (ParseException | DAOException | ConnectionException e) {
			throw new CommandException(e);
		}
		return PAGE;
	}

}
