package edu.training.hotel.command;

import javax.servlet.http.HttpServletRequest;

import edu.training.hotel.exception.CommandException;

public class ShowAuthorizationFormCommand implements HotelCommand {

	private static final String PAGE = "WEB-INF/pages/authorization.jsp";
	
	public ShowAuthorizationFormCommand() {
		
	}

	@Override
	public String process(HttpServletRequest request) throws CommandException {
		return PAGE;
	}

}
