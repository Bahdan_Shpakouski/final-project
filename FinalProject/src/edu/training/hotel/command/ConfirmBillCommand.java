package edu.training.hotel.command;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import edu.training.hotel.dao.ApartmentStatusDAO;
import edu.training.hotel.dao.ApartmentStatusDAOImpl;
import edu.training.hotel.dao.BillDAO;
import edu.training.hotel.dao.BillDAOImpl;
import edu.training.hotel.entity.ApartmentStatus;
import edu.training.hotel.entity.Bill;
import edu.training.hotel.exception.CommandException;
import edu.training.hotel.exception.ConnectionException;
import edu.training.hotel.exception.DAOException;

public class ConfirmBillCommand implements HotelCommand {

	private static final String PAGE = "WEB-INF/pages/bills.jsp";
	private static final String STATUS_CONFIRMED = "Confirmed";
	private static final String STATUS_RESERVED = "Reserved";
	private static final String BILL_ID = "billId";
	private static final String NEW_ONLY = "newOnly";
	private static final String BILL_LIST = "list";
	
	public ConfirmBillCommand() {
		
	}

	@Override
	public String process(HttpServletRequest request) throws CommandException {
		BillDAO billDAO = new BillDAOImpl();
		ArrayList<Bill> list;
		long id = Long.parseLong(request.getParameter(BILL_ID));
		try {
			Bill bill = billDAO.selectBill(id);
			bill.setBillStatus(STATUS_CONFIRMED);
			billDAO.updateBill(id, bill);
			ApartmentStatusDAO statusDAO = new ApartmentStatusDAOImpl();
			ApartmentStatus status = new ApartmentStatus();
			status.setBillId(id);
			status.setApartmentId(bill.getApartmentId());
			status.setClientId(bill.getClientId());
			status.setCurrent(false);
			status.setStatus(STATUS_RESERVED);
			statusDAO.createStatus(status);
			list = billDAO.selectNewBills();
			request.setAttribute(NEW_ONLY, true);
			request.setAttribute(BILL_LIST, list);
		} catch (DAOException | ConnectionException e) {
			throw new CommandException(e);
		}
		return PAGE;
	}

}
