package edu.training.hotel.command;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import edu.training.hotel.dao.ApplicationDAO;
import edu.training.hotel.dao.ApplicationDAOImpl;
import edu.training.hotel.dao.BillDAO;
import edu.training.hotel.dao.BillDAOImpl;
import edu.training.hotel.dao.ClientDAO;
import edu.training.hotel.dao.ClientDAOImpl;
import edu.training.hotel.entity.Application;
import edu.training.hotel.entity.Bill;
import edu.training.hotel.entity.Client;
import edu.training.hotel.exception.CommandException;
import edu.training.hotel.exception.ConnectionException;
import edu.training.hotel.exception.DAOException;

public class ShowClientCommand implements HotelCommand {

	private static final String PAGE = "WEB-INF/pages/clientProfile.jsp";
	private static final String CLIENT_ID = "clientId";
	private static final String MODE = "mode";
	private static final String CLIENT = "client";
	private static final String APPLICATION_LIST = "applicationList";
	private static final String BILL_LIST = "billList";
	private static final String SHOW_FORM = "show_form";
	public ShowClientCommand() {
		
	}

	@Override
	public String process(HttpServletRequest request) throws CommandException {
		ClientDAO clientDAO = new ClientDAOImpl();
		long id = Long.parseLong(request.getParameter(CLIENT_ID));
		int mode = Integer.parseInt(request.getParameter(MODE));
		request.setAttribute(SHOW_FORM, request.getParameter(SHOW_FORM));
		request.setAttribute(MODE, mode);
		try {
			Client client = clientDAO.selectClient(id);
			request.setAttribute(CLIENT, client);
			if(mode == 1){
				ApplicationDAO applicationDAO = new ApplicationDAOImpl();
				ArrayList<Application> list = applicationDAO.selectApplications(id);
				request.setAttribute(APPLICATION_LIST, list);
			}else if(mode == 2){
				BillDAO billDAO = new BillDAOImpl();
				ArrayList<Bill> list = billDAO.selectClientBills(id);
				request.setAttribute(BILL_LIST, list);
			}
		} catch (DAOException | ConnectionException e) {
			throw new CommandException(e);
		}
		return PAGE;
	}

}
