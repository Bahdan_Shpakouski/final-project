package edu.training.hotel.command;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import edu.training.hotel.dao.ClientDAO;
import edu.training.hotel.dao.ClientDAOImpl;
import edu.training.hotel.entity.Client;
import edu.training.hotel.exception.CommandException;
import edu.training.hotel.exception.ConnectionException;
import edu.training.hotel.exception.DAOException;

public class ShowClientsCommand implements HotelCommand{

	private static final String PAGE = "WEB-INF/pages/clients.jsp";
	private static final String CLIENT_LIST = "list";
	
	public ShowClientsCommand() {
		
	}

	@Override
	public String process(HttpServletRequest request) throws CommandException {
		ClientDAO clientDAO = new ClientDAOImpl();
		try {
			ArrayList<Client> list = clientDAO.selectClients();
			request.setAttribute(CLIENT_LIST, list);
		} catch (DAOException | ConnectionException e) {
			throw new CommandException(e);
		}
		return PAGE;
	}

}
