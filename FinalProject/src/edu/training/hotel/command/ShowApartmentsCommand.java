package edu.training.hotel.command;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import edu.training.hotel.dao.ApartmentDAO;
import edu.training.hotel.dao.ApartmentDAOImpl;
import edu.training.hotel.entity.Apartment;
import edu.training.hotel.exception.CommandException;
import edu.training.hotel.exception.ConnectionException;
import edu.training.hotel.exception.DAOException;

public class ShowApartmentsCommand implements HotelCommand {
	
	private static final String PAGE = "WEB-INF/pages/apartments.jsp";
	private static final String APARTMENT_LIST = "list";
	
	public ShowApartmentsCommand() {
		
	}

	@Override
	public String process(HttpServletRequest request) throws CommandException {
		try {
			ApartmentDAO apartmentDAO = new ApartmentDAOImpl();
			ArrayList<Apartment> list = apartmentDAO.selectApartments();
			request.setAttribute(APARTMENT_LIST, list);
		} catch (DAOException | ConnectionException e) {
			throw new CommandException(e.getMessage());
		}
		return PAGE;
	}

}
