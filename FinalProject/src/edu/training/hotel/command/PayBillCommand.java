package edu.training.hotel.command;

import javax.servlet.http.HttpServletRequest;
import edu.training.hotel.dao.BillDAO;
import edu.training.hotel.dao.BillDAOImpl;
import edu.training.hotel.entity.Bill;
import edu.training.hotel.exception.CommandException;
import edu.training.hotel.exception.ConnectionException;
import edu.training.hotel.exception.DAOException;

public class PayBillCommand implements HotelCommand {

	private static final String PAGE = "index.jsp";
	private static final String STATUS = "Payed";
	private static final String BILL_ID = "billId";
	
	public PayBillCommand() {
		
	}

	@Override
	public String process(HttpServletRequest request) throws CommandException {
		BillDAO billDAO = new BillDAOImpl();
		long id = Long.parseLong(request.getParameter(BILL_ID));
		try {
			Bill bill = billDAO.selectBill(id);
			bill.setBillStatus(STATUS);
			billDAO.updateBill(id, bill);
		} catch (DAOException | ConnectionException e) {
			throw new CommandException(e);
		}
		return PAGE;
	}

}
