package edu.training.hotel.command;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import edu.training.hotel.dao.ApartmentDAO;
import edu.training.hotel.dao.ApartmentDAOImpl;
import edu.training.hotel.dao.HistoryRecordDAO;
import edu.training.hotel.dao.HistoryRecordDAOImpl;
import edu.training.hotel.entity.HistoryRecord;
import edu.training.hotel.exception.CommandException;
import edu.training.hotel.exception.ConnectionException;
import edu.training.hotel.exception.DAOException;

public class ShowHistoryCommand implements HotelCommand {

	private static final String PAGE = "WEB-INF/pages/history.jsp";
	private static final String RECORD_LIST = "list";
	private static final String APARTMENT_ID = "apartmentId";
	private static final String ROOM = "room";
	
	public ShowHistoryCommand() {
		
	}

	@Override
	public String process(HttpServletRequest request) throws CommandException {
		HistoryRecordDAO historyRecordDAO = new HistoryRecordDAOImpl();
		long apartmentId = Long.parseLong(request.getParameter(APARTMENT_ID));
		ApartmentDAO apartmentDAO = new ApartmentDAOImpl();
		try {
			ArrayList<HistoryRecord> list =  historyRecordDAO.selectHistory(apartmentId);
			request.setAttribute(ROOM, apartmentDAO
					.selectApartment(apartmentId).getApartmentNumber());
			request.setAttribute(RECORD_LIST, list);
		} catch (DAOException | ConnectionException e) {
			throw new CommandException(e);
		}
		return PAGE;
	}

}
