package edu.training.hotel.command;

import javax.servlet.http.HttpServletRequest;

import edu.training.hotel.dao.ApplicationDAO;
import edu.training.hotel.dao.ApplicationDAOImpl;
import edu.training.hotel.exception.CommandException;
import edu.training.hotel.exception.ConnectionException;
import edu.training.hotel.exception.DAOException;

public class DeleteApplicationCommand implements HotelCommand {

	private static final String PAGE ="index.jsp";
	private static final String APPLICATION_ID = "applicationId";
	
	public DeleteApplicationCommand() {
		
	}

	@Override
	public String process(HttpServletRequest request) throws CommandException {
		ApplicationDAO applicationDAO = new ApplicationDAOImpl();
		long id = Long.parseLong(request.getParameter(APPLICATION_ID));
		try {
			applicationDAO.deleteApplication(id);
		} catch (DAOException | ConnectionException e) {
			throw new CommandException(e);
		}
		return PAGE;
	}

}
