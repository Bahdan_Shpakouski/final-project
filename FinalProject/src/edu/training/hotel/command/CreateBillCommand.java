package edu.training.hotel.command;

import java.util.Calendar;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import edu.training.hotel.dao.ApartmentDAO;
import edu.training.hotel.dao.ApartmentDAOImpl;
import edu.training.hotel.dao.ApplicationDAO;
import edu.training.hotel.dao.ApplicationDAOImpl;
import edu.training.hotel.dao.BillDAO;
import edu.training.hotel.dao.BillDAOImpl;
import edu.training.hotel.entity.Application;
import edu.training.hotel.entity.Bill;
import edu.training.hotel.exception.CommandException;
import edu.training.hotel.exception.ConnectionException;
import edu.training.hotel.exception.DAOException;

public class CreateBillCommand implements HotelCommand {

	private static final String PAGE = "index.jsp";
	private static final String STATUS_NEW = "New";
	private static final String STATUS_SERVED = "Served";
	private static final String APPLICATION_ID = "applicationId";
	private static final String CLIENT_ID = "clientId";
	private static final String APARTMENT_ID = "apartmentId";
	
	static Logger logger = Logger.getLogger(ServeApplicationCommand.class);
	
	public CreateBillCommand() {
		
	}

	@Override
	public String process(HttpServletRequest request) throws CommandException {
		BillDAO billDAO = new BillDAOImpl();
		ApartmentDAO apartmentDAO = new ApartmentDAOImpl();
		ApplicationDAO applicationDAO = new ApplicationDAOImpl();
		Bill bill = new Bill();
		try {
			long clientId = Long.parseLong(request.getParameter(CLIENT_ID));
			long apartmentId = Long.parseLong(request.getParameter(APARTMENT_ID));
			long applicationId = Long.parseLong(request.getParameter(APPLICATION_ID));
			int dayCost = apartmentDAO.selectApartment(apartmentId).getCost();
			Application application = applicationDAO.selectApplication(applicationId);
			int totalCost = dayCost * application.getNumberOfDays();
			Calendar c = Calendar.getInstance(); 
			c.setTime(application.getArrivalDate()); 
			c.add(Calendar.DATE, application.getNumberOfDays());
			Date departureDate = c.getTime();
			bill.setClientId(clientId);
			bill.setApartmentId(apartmentId);
			bill.setCost(totalCost);
			bill.setMovingInDate(application.getArrivalDate());
			bill.setMovingOutDate(departureDate);
			bill.setBillStatus(STATUS_NEW);
			logger.info(bill.toString());
			billDAO.createBill(bill);
			application.setApplicationStatus(STATUS_SERVED);
			applicationDAO.updateApplication(applicationId, application);
		} catch (DAOException | ConnectionException e) {
			throw new CommandException(e.getMessage());
		}
		return PAGE;
	}

}
