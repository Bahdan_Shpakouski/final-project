package edu.training.hotel.command;

import javax.servlet.http.HttpServletRequest;

import edu.training.hotel.dao.ApartmentDAO;
import edu.training.hotel.dao.ApartmentDAOImpl;
import edu.training.hotel.entity.Apartment;
import edu.training.hotel.exception.CommandException;
import edu.training.hotel.exception.ConnectionException;
import edu.training.hotel.exception.DAOException;

public class ShowApartmentCommand implements HotelCommand {
	
	private static final String PAGE = "WEB-INF/pages/apartment.jsp";
	private static final String ID = "id";
	private static final String APARTMENT = "apartment";
	private static final String SHOW_FORM = "show_form";
	
	public ShowApartmentCommand() {
		
	}

	@Override
	public String process(HttpServletRequest request) throws CommandException {
		try {
			ApartmentDAO apartmentDAO = new ApartmentDAOImpl();
			long id = Long.parseLong(request.getParameter(ID));
			boolean showForm = Boolean.parseBoolean(request.getParameter(SHOW_FORM));
			request.setAttribute(SHOW_FORM, showForm);
			Apartment apartment = apartmentDAO.selectApartment(id);
			request.setAttribute(APARTMENT, apartment);
		} catch (DAOException | ConnectionException e) {
			throw new CommandException(e);
		}
		return PAGE;
	}

}
