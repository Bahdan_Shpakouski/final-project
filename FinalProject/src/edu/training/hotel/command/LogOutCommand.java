package edu.training.hotel.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import edu.training.hotel.exception.CommandException;

public class LogOutCommand implements HotelCommand {

	private static final String PAGE = "index.jsp";
	private static final String LANGUAGE = "lang";
	private static final String LOCALE_RU = "ru_RU";
	private static final String LOCALE_EN = "en_US";
	
	public LogOutCommand() {
		
	}

	@Override
	public String process(HttpServletRequest request) throws CommandException {
		HttpSession session = request.getSession();
		String locale = (String) session.getAttribute(LANGUAGE);
		session.invalidate();
		session = request.getSession();
		if(locale.equals(LOCALE_RU)){
			session.setAttribute(LANGUAGE, LOCALE_RU);
		}else if(locale.equals(LOCALE_EN)){
			session.setAttribute(LANGUAGE, LOCALE_EN);
		}
		return PAGE;
	}

}
