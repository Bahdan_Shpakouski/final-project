package edu.training.hotel.command;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import edu.training.hotel.dao.ApplicationDAO;
import edu.training.hotel.dao.ApplicationDAOImpl;
import edu.training.hotel.entity.Application;
import edu.training.hotel.exception.CommandException;
import edu.training.hotel.exception.ConnectionException;
import edu.training.hotel.exception.DAOException;

public class CreateApplicationCommand implements HotelCommand {

	private static final String PAGE = "index.jsp";
	private static final String STATUS = "New";
	private static final String CLIENT_ID = "clientId";
	private static final String NUMBER_OF_BEDS = "number_of_beds";
	private static final String NUMBER_OF_PEOPLE = "number_of_people";
	private static final String ARRIVAL_DATE = "date";
	private static final String NUMBER_OF_DAYS = "number_of_days";
	private static final String SELECT_CONTROL = "select_control";
	
	static Logger logger = Logger.getLogger(CreateApplicationCommand.class);
	
	public CreateApplicationCommand() {
		
	}

	@Override
	public String process(HttpServletRequest request) throws CommandException {
		Application application = new Application();
		ApplicationDAO applicationDAO = new ApplicationDAOImpl();
		try{
			long clientId = Long.parseLong(request.getParameter(CLIENT_ID));
			application.setClientId(clientId);
			String numberOfBeds;
			if(!(numberOfBeds = request.getParameter(NUMBER_OF_BEDS)).isEmpty()){
				logger.info("beds:" + numberOfBeds);
				application.setNumberOfBeds(Integer.parseInt(numberOfBeds));
			}
			int numberOfPeople = Integer.parseInt(request
					.getParameter(NUMBER_OF_PEOPLE));
			application.setNumberOfPeople(numberOfPeople);
			logger.info("people:" + numberOfPeople);
			String arrivalDate = request.getParameter(ARRIVAL_DATE);
			DateFormat format = new SimpleDateFormat("dd-MM-yyyy");
			Date date = format.parse(arrivalDate);
			application.setArrivalDate(date);
			logger.info("date:" + date );
			int numberOfDays = Integer.parseInt(request.getParameter(NUMBER_OF_DAYS));
			application.setNumberOfDays(numberOfDays);
			logger.info("days:" + numberOfDays);
			String type = request.getParameter(SELECT_CONTROL);
			application.setAppartmentType(type);
			logger.info("type:" + type);
			application.setApplicationDate(new Date());
			application.setApplicationStatus(STATUS);
			applicationDAO.createApplication(application);
		} catch (ParseException | DAOException | ConnectionException e) {
			throw new CommandException(e);
		}
		return PAGE;
	}

}
