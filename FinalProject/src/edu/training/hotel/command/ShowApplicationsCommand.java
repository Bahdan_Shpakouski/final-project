package edu.training.hotel.command;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import edu.training.hotel.dao.ApplicationDAO;
import edu.training.hotel.dao.ApplicationDAOImpl;
import edu.training.hotel.entity.Application;
import edu.training.hotel.exception.CommandException;
import edu.training.hotel.exception.ConnectionException;
import edu.training.hotel.exception.DAOException;

public class ShowApplicationsCommand implements HotelCommand {

	private static final String PAGE = "WEB-INF/pages/applications.jsp";
	private static final String NEW_ONLY = "newOnly";
	private static final String APPLICATION_LIST = "list";
	
	public ShowApplicationsCommand() {
		
	}

	@Override
	public String process(HttpServletRequest request) throws CommandException {
		ApplicationDAO applicationDAO = new ApplicationDAOImpl();
		boolean newOnly = Boolean.parseBoolean(request
				.getParameter(NEW_ONLY));
		ArrayList<Application> list;
		try {
			if(newOnly){
				list = applicationDAO.selectNewApplications();
				request.setAttribute(NEW_ONLY, true);
			}else{
				list = applicationDAO.selectApplications();
				request.setAttribute(NEW_ONLY, false);
			}
			request.setAttribute(APPLICATION_LIST, list);
		} catch (DAOException | ConnectionException e) {
			throw new CommandException(e);
		}
		return PAGE;
	}

}
