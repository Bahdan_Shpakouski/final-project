package edu.training.hotel.command;

import javax.servlet.http.HttpServletRequest;

import edu.training.hotel.exception.CommandException;

public interface HotelCommand {

	String process(HttpServletRequest request) throws CommandException;
}
