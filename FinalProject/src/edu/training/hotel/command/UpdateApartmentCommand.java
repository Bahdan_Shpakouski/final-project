package edu.training.hotel.command;

import javax.servlet.http.HttpServletRequest;

import edu.training.hotel.dao.ApartmentDAO;
import edu.training.hotel.dao.ApartmentDAOImpl;
import edu.training.hotel.entity.Apartment;
import edu.training.hotel.exception.CommandException;
import edu.training.hotel.exception.ConnectionException;
import edu.training.hotel.exception.DAOException;

public class UpdateApartmentCommand implements HotelCommand {

	private static final String PAGE = "WEB-INF/pages/apartment.jsp";
	private static final String ID = "id";
	private static final String HAS_TV = "hasTV";
	private static final String HAS_FRIDGE = "hasFridge";
	private static final String HAS_AIR_CONDITIONER = "hasAirConditioner";
	private static final String APARTMENT = "apartment";
	private static final String SHOW_FORM = "show_form";
	
	public UpdateApartmentCommand() {
		
	}

	@Override
	public String process(HttpServletRequest request) throws CommandException {
		ApartmentDAO apartmentDAO = new ApartmentDAOImpl();
		long id = Long.parseLong(request.getParameter(ID));
		try {
			boolean hasTV = Boolean.parseBoolean(request.getParameter(HAS_TV));
			boolean hasFridge = Boolean.parseBoolean(request.getParameter(HAS_FRIDGE));
			boolean hasAirConditioner = Boolean.parseBoolean(request.getParameter(HAS_AIR_CONDITIONER));
			apartmentDAO.updateAdditionalInformation(id, hasTV, hasFridge, hasAirConditioner);
			request.setAttribute(SHOW_FORM, false);
			Apartment apartment = apartmentDAO.selectApartment(id);
			request.setAttribute(APARTMENT, apartment);
		} catch (DAOException | ConnectionException e) {
			throw new CommandException(e);
		}
		return PAGE;
	}

}
