package edu.training.hotel.command;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import edu.training.hotel.dao.ApartmentStatusDAO;
import edu.training.hotel.dao.ApartmentStatusDAOImpl;
import edu.training.hotel.dao.BillDAO;
import edu.training.hotel.dao.BillDAOImpl;
import edu.training.hotel.entity.ApartmentStatus;
import edu.training.hotel.entity.Bill;
import edu.training.hotel.entity.HistoryRecord;
import edu.training.hotel.exception.CommandException;
import edu.training.hotel.exception.ConnectionException;
import edu.training.hotel.exception.DAOException;
import edu.training.hotel.dao.HistoryRecordDAO;
import edu.training.hotel.dao.HistoryRecordDAOImpl;

public class SetExpiredStatusCommand implements HotelCommand {

	private static final String PAGE = "WEB-INF/pages/statuses.jsp";
	private static final String STATUS_ID = "statusId";
	private static final String STATUS_EXPIRED = "Expired";
	private static final String STATUS_LIST = "list";
	private static final String MODE = "mode";
	private static final String BILLS = "bills";
	private static final String DATE = "today";
	
	public SetExpiredStatusCommand() {
		
	}

	@Override
	public String process(HttpServletRequest request) throws CommandException {
		long id = Long.parseLong(request.getParameter(STATUS_ID));
		ApartmentStatusDAO statusDAO = new ApartmentStatusDAOImpl();
		HistoryRecordDAO recordDAO = new HistoryRecordDAOImpl();
		BillDAO billDAO = new BillDAOImpl();
		ArrayList<ApartmentStatus> statusList = null;
		ArrayList<Bill> bills = new ArrayList<Bill>();
		try {
			ApartmentStatus status = statusDAO.selectStatus(id);
			HistoryRecord record = new HistoryRecord();
			long apartmentId = status.getApartmentId();
			record.setApartmentId(apartmentId);
			record.setClientId(status.getClientId());
			record.setMovingInDate(billDAO.selectBill(status.getBillId())
					.getMovingInDate());
			record.setMovingOutDate(new Date());
			recordDAO.createRecord(record);
			status.setCurrent(false);
			status.setStatus(STATUS_EXPIRED);
			statusDAO.updateStatus(id, status);
			statusDAO.setCurrent(apartmentId, true);
			statusList = statusDAO.selectCurrentStatuses();
			for( int i = 0; i <statusList.size() ; i++){
				long billId = statusList.get(i).getBillId();
				bills.add(billDAO.selectBill(billId));
			}
			Calendar c = Calendar.getInstance();
			c.set(Calendar.HOUR_OF_DAY, 0);
		    c.set(Calendar.MINUTE, 0);
		    c.set(Calendar.SECOND, 0);
		    c.set(Calendar.MILLISECOND, 0);
		    Date date = c.getTime();
			request.setAttribute(MODE, 0);
			request.setAttribute(BILLS, bills);
			request.setAttribute(DATE, date);
			request.setAttribute(STATUS_LIST, statusList);
		} catch (DAOException | ConnectionException e) {
			throw new CommandException(e);
		}
		return PAGE;
	}

}
