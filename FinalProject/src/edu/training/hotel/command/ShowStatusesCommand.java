package edu.training.hotel.command;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import edu.training.hotel.dao.ApartmentStatusDAO;
import edu.training.hotel.dao.ApartmentStatusDAOImpl;
import edu.training.hotel.dao.BillDAO;
import edu.training.hotel.dao.BillDAOImpl;
import edu.training.hotel.entity.ApartmentStatus;
import edu.training.hotel.entity.Bill;
import edu.training.hotel.exception.CommandException;
import edu.training.hotel.exception.ConnectionException;
import edu.training.hotel.exception.DAOException;

public class ShowStatusesCommand implements HotelCommand {

	private static final String PAGE = "WEB-INF/pages/statuses.jsp";
	private static final String STATUS_LIST = "list";
	private static final String MODE = "mode";
	private static final String BILLS = "bills";
	private static final String DATE = "today";
	
	public ShowStatusesCommand() {
		
	}

	@Override
	public String process(HttpServletRequest request) throws CommandException {
		ApartmentStatusDAO statusDAO = new ApartmentStatusDAOImpl();
		BillDAO billDAO = new BillDAOImpl();
		int mode = Integer.parseInt(request.getParameter(MODE));
		ArrayList<ApartmentStatus> statusList = null;
		ArrayList<Bill> bills = new ArrayList<Bill>();
		try{	
			if(mode == 0){
				statusList = statusDAO.selectCurrentStatuses();
			} else if (mode == 1){
				statusList = statusDAO.selectReservedStatuses();
			} else if (mode == 2){
				statusList = statusDAO.selectExpiredStatuses();
			}
			for( int i = 0; i <statusList.size() ; i++){
				long id = statusList.get(i).getBillId();
				bills.add(billDAO.selectBill(id));
			}
			Calendar c = Calendar.getInstance();
			c.set(Calendar.HOUR_OF_DAY, 0);
		    c.set(Calendar.MINUTE, 0);
		    c.set(Calendar.SECOND, 0);
		    c.set(Calendar.MILLISECOND, 0);
		    Date date = c.getTime();
			request.setAttribute(MODE, mode);
			request.setAttribute(BILLS, bills);
			request.setAttribute(DATE, date);
			request.setAttribute(STATUS_LIST, statusList);
		} catch (DAOException | ConnectionException e) {
			throw new CommandException(e);
		}
		return PAGE;
	}

}
