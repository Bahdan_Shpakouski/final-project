package edu.training.hotel.command;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import edu.training.hotel.dao.ClientDAO;
import edu.training.hotel.dao.ClientDAOImpl;
import edu.training.hotel.entity.Client;
import edu.training.hotel.exception.CommandException;
import edu.training.hotel.exception.ConnectionException;
import edu.training.hotel.exception.DAOException;

public class CreateClientCommand implements HotelCommand {

	private static final String PAGE = "index.jsp";
	private static final String NAME = "name";
	private static final String SURNAME = "surname";
	private static final String AGE = "age";
	private static final String GENDER = "gender";
	private static final String COUNTRY = "country";
	private static final String EMAIL = "email";
	private static final String PASSWORD = "password";
	private static final String ENCODING = "UTF-8";
	private static final String ENCRYPTION = "MD5";
	private static final String ROLE = "role";
	private static final String ROLE_CLIENT = "client";
	private static final String CLIENT_ID = "clientId";
	
	public CreateClientCommand() {
		
	}

	@Override
	public String process(HttpServletRequest request) throws CommandException {
		ClientDAO clientDAO = new ClientDAOImpl();
		Client client = new Client();
		String password = request.getParameter(PASSWORD);
		String encryptedPassword;
		try {
			MessageDigest md = MessageDigest.getInstance(ENCRYPTION);
			md.update(password.getBytes(ENCODING));
			byte[] digest = md.digest();
			StringBuffer sb = new StringBuffer();
			for (byte b : digest) {
				sb.append(String.format("%02x", b & 0xff));
			}
			encryptedPassword = sb.toString();
		} catch (NoSuchAlgorithmException | UnsupportedEncodingException e1) {
			throw new CommandException("���������� �� �������");
		}
		try {
			client.setName(request.getParameter(NAME));
			client.setSurname(request.getParameter(SURNAME));
			client.setAge(Integer.parseInt(request.getParameter(AGE)));
			client.setGender(request.getParameter(GENDER));
			client.setCountry(request.getParameter(COUNTRY));
			client.seteMail(request.getParameter(EMAIL));
			client.setPassword(encryptedPassword);
			clientDAO.createClient(client);
			long id = clientDAO.authorize(client);
			if(id != 0){
				HttpSession session = request.getSession(true);
				session.setAttribute(ROLE, ROLE_CLIENT);
				session.setAttribute(CLIENT_ID, id);			
			}else {
				throw new CommandException("����������� �� �������");
			}
		} catch (DAOException | ConnectionException e) {
			throw new CommandException(e);
		}
		return PAGE;
	}

}
