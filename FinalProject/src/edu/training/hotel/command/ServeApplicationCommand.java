package edu.training.hotel.command;

import java.util.Calendar;
import java.util.ArrayList;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import edu.training.hotel.dao.ApartmentDAO;
import edu.training.hotel.dao.ApartmentDAOImpl;
import edu.training.hotel.dao.ApartmentStatusDAO;
import edu.training.hotel.dao.ApartmentStatusDAOImpl;
import edu.training.hotel.dao.ApplicationDAO;
import edu.training.hotel.dao.ApplicationDAOImpl;
import edu.training.hotel.entity.Apartment;
import edu.training.hotel.entity.ApartmentStatus;
import edu.training.hotel.entity.Application;
import edu.training.hotel.exception.CommandException;
import edu.training.hotel.exception.ConnectionException;
import edu.training.hotel.exception.DAOException;

public class ServeApplicationCommand implements HotelCommand {

	private static final String PAGE = "WEB-INF/pages/serveApplication.jsp";
	private static final String STATUS = "Free";
	private static final String APPLICATION_ID = "applicationId";
	private static final String APPLICATION = "application";
	private static final String APARTMENT_LIST = "apartmentList";
	private static final String STATUS_LIST = "statusList";
	
	public ServeApplicationCommand() {
		
	}

	@Override
	public String process(HttpServletRequest request) throws CommandException {
		try{
			ApartmentDAO apartmentDAO = new ApartmentDAOImpl();
			ApplicationDAO applicationDAO = new ApplicationDAOImpl();
			ApartmentStatusDAO statusDAO = new ApartmentStatusDAOImpl();
			long id = Long.parseLong(request.getParameter(APPLICATION_ID));
			Application application = applicationDAO.selectApplication(id);
			request.setAttribute(APPLICATION, application);
			ArrayList<Apartment> list = apartmentDAO.selectApartments();
			request.setAttribute(APARTMENT_LIST, list);
			Calendar c = Calendar.getInstance(); 
			c.setTime(application.getArrivalDate()); 
			c.add(Calendar.DATE, application.getNumberOfDays());
			Date departureDate = c.getTime();
			ArrayList<ApartmentStatus> statusList 
				= new ArrayList<ApartmentStatus>();
			for(int i = 0; i < list.size() ; i++) {
				ApartmentStatus status = statusDAO
						.selectStatusForApartment(list.get(i).getApartmentId(),
						application.getArrivalDate(), departureDate);
				if(status == null){
					status = new ApartmentStatus();
					status.setStatus(STATUS);
				}
				statusList.add(status);
			}
			request.setAttribute(STATUS_LIST, statusList);
		} catch (DAOException | ConnectionException e) {
			throw new CommandException(e);
		}
		return PAGE;
	}
}
