package edu.training.hotel.command;

import javax.servlet.http.HttpServletRequest;

import edu.training.hotel.dao.ApplicationDAO;
import edu.training.hotel.dao.ApplicationDAOImpl;
import edu.training.hotel.entity.Application;
import edu.training.hotel.exception.CommandException;
import edu.training.hotel.exception.ConnectionException;
import edu.training.hotel.exception.DAOException;

public class ShowUpdateApplicationFormCommand implements HotelCommand {

	private static final String PAGE = "WEB-INF/pages/updateApplication.jsp";
	private static final String APPLICATION_ID = "applicationId";
	private static final String APPLICATION = "application";
	
	public ShowUpdateApplicationFormCommand() {
		
	}

	@Override
	public String process(HttpServletRequest request) throws CommandException {
		long applicationId = Long.parseLong(request.getParameter(APPLICATION_ID));
		ApplicationDAO applicationDAO = new ApplicationDAOImpl();
		try {
			Application application = applicationDAO.selectApplication(applicationId);
			request.setAttribute(APPLICATION, application);
		} catch (DAOException | ConnectionException e) {
			throw new CommandException(e);
		}
		return PAGE;
	}

}
