package edu.training.hotel.connectionpool;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.concurrent.ArrayBlockingQueue;

import org.apache.log4j.Logger;

import edu.training.hotel.exception.ConnectionException;
import edu.training.hotel.path.Path;

import java.util.Properties;

public class ProxyConnectionPool {
	
	private ArrayBlockingQueue<ProxyConnection> proxyConnections;

	private static final int CONNECTION_NUMBER = 10;
	private static final ProxyConnectionPool INSTANCE;
	private static final String PROPERTY_PATH = Path.getPropertiesPath()+"\\database.properties";
	private static final String URL = "url";
	private static final String DRIVER = "driver";
	private static final String USER = "user";
	private static final String PASSWORD = "password";
	
	static Logger logger = Logger.getLogger(ProxyConnectionPool.class);

    static {
        try {
        	INSTANCE = new ProxyConnectionPool();
        } catch (ConnectionException e) {
        	logger.fatal(e.getMessage());
            throw new ExceptionInInitializerError();
        }
    }
	
	private ProxyConnectionPool() throws ConnectionException {
		proxyConnections = new ArrayBlockingQueue<ProxyConnection>(CONNECTION_NUMBER);
		Properties props = new Properties();
		String url;
		String driver;
		String user;
		String password;
		try(FileInputStream in = new FileInputStream(PROPERTY_PATH)) {
			props.load(in);
			url = props.getProperty(URL);
			driver = props.getProperty(DRIVER);
			user = props.getProperty(USER);
			password = props.getProperty(PASSWORD);
			Class.forName(driver).newInstance();
		} catch (FileNotFoundException | InstantiationException 
				| IllegalAccessException | ClassNotFoundException e) {
			throw new ConnectionException(e);
		} catch (IOException e) {
			throw new ConnectionException(e);
		} 
		for(int i =0; i<CONNECTION_NUMBER; i++){
			try {
				proxyConnections.add(new ProxyConnection(DriverManager
						.getConnection(url, user, password)));
			} catch (SQLException e) {
				throw new ConnectionException(e.getMessage());
			}
		}
	}
	
	public static ProxyConnectionPool getInstance(){
		return INSTANCE;
	}

	public ProxyConnection getConnection() throws ConnectionException{
		if(proxyConnections.isEmpty()){
			throw new ConnectionException("��� ��������� ����������");
		}
		ProxyConnection pc;
		try {
			pc = proxyConnections.take();
		} catch (InterruptedException e) {
			throw new ConnectionException(e);
		}
		return pc;
	}
	
	public void putConnection(ProxyConnection connection) {
		proxyConnections.add(connection);
	}
	
	public void closeConnections() throws ConnectionException{
		while(!proxyConnections.isEmpty()){
			try {
				proxyConnections.remove().closeConnection();
			} catch (SQLException e) {
				throw new ConnectionException(e.getMessage());
			}
		}
	}
}
