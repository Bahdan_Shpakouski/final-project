package edu.training.hotel.contextlistener;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import edu.training.hotel.connectionpool.ProxyConnectionPool;
import edu.training.hotel.exception.ConnectionException;
import edu.training.hotel.path.Path;

/**
 * Application Lifecycle Listener implementation class HotelContextListener
 *
 */
@WebListener
public class HotelContextListener implements ServletContextListener {

	static Logger logger;
	
    /**
     * Default constructor. 
     */
    public HotelContextListener() {}

	/**
     * @see ServletContextListener#contextDestroyed(ServletContextEvent)
     */
    public void contextDestroyed(ServletContextEvent sce)  {
    	logger.info("Closing connections");
    	try {
			ProxyConnectionPool.getInstance().closeConnections();
		}catch (ConnectionException e) {
			logger.error("Error occuried while closing pool connections: "
					+ e.getMessage());
		}
    	logger.info("Shutting down");
    }

	/**
     * @see ServletContextListener#contextInitialized(ServletContextEvent)
     */
    public void contextInitialized(ServletContextEvent sce)  { 
    	Path.setPropertiesPath(sce.getServletContext()
				.getRealPath("/WEB-INF/classes/properties/"));
		PropertyConfigurator.configure(Path.getPropertiesPath() 
				+ "/log4j.properties");
		logger = Logger.getLogger(HotelContextListener.class);
		logger.info("Starting up");
    }
	
}
