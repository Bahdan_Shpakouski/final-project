<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="ctg" uri="/WEB-INF/tld/hotelTags.tld" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<fmt:setLocale value="${lang}"/>
<fmt:setBundle basename="edu.training.hotel.localization.HotelBundle"/>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title><fmt:message key="serve.title"/></title>
	<link rel="stylesheet" type="text/css" href="styles/list-style.css">
	<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
 	<script src="//code.jquery.com/jquery-1.10.2.js"></script>
 	<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
 	<link rel="stylesheet" href="/resources/demos/style.css">
  	<script>
  	$(function() {
    	$( "#datepicker" ).datepicker({ minDate: 0, maxDate: "+6M", dateFormat: "dd-mm-yy" });
  	});
 	</script>
</head>
<body>
	<ctg:home/>
	<table border = "1" align="center">
		<caption><fmt:message key="serve.application"/></caption>
		<tr>
			<th>Id</th>
			<th><fmt:message key="applications.people"/></th>
			<th><fmt:message key="applications.beds"/></th>
			<th><fmt:message key="applications.arrival"/></th>
			<th><fmt:message key="applications.days"/></th>
			<th><fmt:message key="applications.type"/></th>
			<th><fmt:message key="clients.clientId"/></th>
			<th><fmt:message key="applications.applicationDate"/></th>
			<th><fmt:message key="applications.status"/></th>
		</tr>
		<tr>
			<td><c:out value="${application.getApplicationId()}"></c:out></td>
			<td><c:out value="${application.getNumberOfPeople()}"></c:out></td>
			<td><c:out value="${application.getNumberOfBeds()}"></c:out></td>
			<td><c:out value="${application.getArrivalDate()}"></c:out></td>
			<td><c:out value="${application.getNumberOfDays()}"></c:out></td>
			<td><c:out value="${application.getAppartmentType()}"></c:out></td>
			<td><c:out value="${application.getClientId()}"></c:out></td>
			<td><c:out value="${application.getApplicationDate()}"></c:out></td>
			<td><c:out value="${application.getApplicationStatus()}"></c:out></td>
		</tr>
	</table>
	<br>
	<br>
	<h4 align="center"><fmt:message key="serve.title"/></h4>
	<ul>
		<c:forEach items="${requestScope.apartmentList}" var="apartment" varStatus="statusIndex">
			<li>
				<fmt:message key="apartment.number"/><c:out value="${apartment.getApartmentNumber()}."></c:out>
				<fmt:message key="apartment.characteristics"/><c:out value="${apartment.getFloor()}"></c:out> 
				<fmt:message key="apartment.floor"/><c:out value="${apartment.getNumberOfRooms()}"></c:out> 
				<fmt:message key="apartment.rooms"/><c:out value="${apartment.getNumberOfPlaces()}"></c:out>
				<fmt:message key="apartment.places"/>
				<c:choose>
					<c:when test="${apartment.getNumberOfBeds()==1}">
						<fmt:message key="apartment.1bed"/>
					</c:when>
					<c:otherwise>
						<fmt:message key="apartment.with"/><c:out value="${apartment.getNumberOfBeds()}"></c:out>
							<fmt:message key="apartment.2beds"/>
					</c:otherwise>
				</c:choose>
				<br>
				<fmt:message key="apartment.type"/>
				<c:choose>
					<c:when test="${apartment.getApartmentType()== 'Standart'}">
						<fmt:message key="apartment.standart"/>
					</c:when>
					<c:when test="${apartment.getApartmentType()== 'Balcony'}">
						<fmt:message key="apartment.balcony"/>
					</c:when>
					<c:when test="${apartment.getApartmentType()== 'Superior'}">
						<fmt:message key="apartment.superior"/>
					</c:when>
					<c:when test="${apartment.getApartmentType()== 'Suite'}">
						<fmt:message key="apartment.suite"/>
					</c:when>
					<c:when test="${apartment.getApartmentType()== 'De Luxe'}">
						<fmt:message key="apartment.luxe"/>
					</c:when>
				</c:choose>
				<br>
				<c:out value="Стоимость дня проживания: ${apartment.getCost()}$."></c:out><br>
				<c:out value="Статус: ${statusList.get(statusIndex.index).getStatus()}"></c:out>
				<form action="HotelServlet">
					<input type="hidden" name="id" value="${apartment.getApartmentId()}">
					<button name="command" value="show_apartment"><fmt:message key="apartments.info"/></button>
					<c:choose>
						<c:when test="${statusList.get(statusIndex.index).getClientId() !=0}">
							<input type="hidden" name="clientId" value="${statusList.get(statusIndex.index).getClientId()}">
							<input type="hidden" name="mode" value="0">
							<button name="command" value="show_client"><fmt:message key="clients.clientProfile"/></button>
						</c:when>
					</c:choose>
				</form>
			</li>		
			<hr>
		</c:forEach>
	</ul>
	<form action="HotelServlet" method="post">
		<input type="hidden" name="clientId" value="${application.getClientId()}">
		<input type="hidden" name="applicationId" value="${application.getApplicationId()}">
		<fmt:message key="history.select"/>: <select name="apartmentId">
					<option value="1">101</option>
					<option value="2">102</option>
					<option value="3">103</option>
					<option value="4">104</option>
					<option value="5">105</option>
					<option value="6">106</option>
					<option value="7">201</option>
					<option value="8">202</option>
					<option value="9">203</option>
					<option value="10">204</option>
					<option value="11">205</option>
					<option value="12">206</option>
				</select>
		<button name="command" value="create_bill"><fmt:message key="serve.newBill"/></button>
		<button name="command" value="decline_application"><fmt:message key="serve.declineApplication"/></button>
	</form>
</body>
</html>