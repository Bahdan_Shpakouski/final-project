<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="ctg" uri="/WEB-INF/tld/hotelTags.tld" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<fmt:setLocale value="${lang}"/>
<fmt:setBundle basename="edu.training.hotel.localization.HotelBundle"/>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title><fmt:message key='registration.title'/></title>
	<link rel="stylesheet" type="text/css" href="styles/main-style.css">
	<script language="javascript">
		function checkPassword() {
			 var password1 = document.getElementById("pass1").value;
			 var password2 = document.getElementById("pass2").value;
			 if(password1 == password2) {
				 document.getElementById("password_status").innerHTML = "";        
			 }else {
				 document.getElementById("password_status").innerHTML = "<fmt:message key='registration.passwordMessage'/>";  
			 }
		}
	</script>
</head>
<body>
	<c:set var="status" scope="page" value="false"></c:set>
	<ctg:home/>
	<div class="borderForm">
		<form action="HotelServlet" method="post">
			<fmt:message key="clients.name"/>: <input type="text" name="name" required>
			<fmt:message key="clients.surname"/>: <input type="text" name="surname" required>
			<fmt:message key="clients.age"/>: <input type="number" name="age" maxlength="3" min="12" max="110" required><br>
			<fmt:message key="clients.gender"/>: <select name="gender">
					<option value="Male"><fmt:message key="client.male"/></option>
					<option value="Female"><fmt:message key="client.female"/></option>
				</select>
			<fmt:message key="clients.country"/>: <input type="text" name="country" required>
			<fmt:message key="clients.email"/>: <input type="email" name="email"><br>
			<fmt:message key="registration.password"/><input type="password" name="password" 
				id="pass1" onChange="checkPassword()" required>
			<fmt:message key="registration.password2"/><input type="password" id="pass2" onChange="checkPassword()" required><br>
			<p id="password_status"></p>
			<button name="command" value="create_client"><fmt:message key="apartment.send"/></button>
		</form>
	</div>
</body>
</html>