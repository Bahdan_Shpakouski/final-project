<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="ctg" uri="/WEB-INF/tld/hotelTags.tld" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<fmt:setLocale value="${lang}"/>
<fmt:setBundle basename="edu.training.hotel.localization.HotelBundle"/>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title><fmt:message key="clients.title"/></title>
	<link rel="stylesheet" type="text/css" href="styles/Style.css">
</head>
<body>
	<ctg:home/>
	<table border = "1" align="center">
		<caption><font size="6"><fmt:message key="clients.table"/></font></caption>
		<tr>
			<th>Id</th>
			<th><fmt:message key="clients.name"/></th>
			<th><fmt:message key="clients.surname"/></th>
			<th><fmt:message key="clients.age"/></th>
			<th><fmt:message key="clients.gender"/></th>
			<th><fmt:message key="clients.country"/></th>
			<th><fmt:message key="clients.email"/></th>
			<th></th>
		</tr>
		<c:forEach items="${requestScope.list}" var="client">
			<tr>
				<td><c:out value="${client.getClientId()}"></c:out></td>
				<td><c:out value="${client.getName()}"></c:out></td>
				<td><c:out value="${client.getSurname()}"></c:out></td>
				<td><c:out value="${client.getAge()}"></c:out></td>
				<td><c:out value="${client.getGender()}"></c:out></td>
				<td><c:out value="${client.getCountry()}"></c:out></td>
				<td><c:out value="${client.geteMail()}"></c:out></td>
				<td><form action="HotelServlet">
					<input type="hidden" name="clientId" value="${client.getClientId()}">
					<input type="hidden" name="mode" value="0" >
					<button name="command" value="show_client"><fmt:message key="clients.clientProfile"/></button>
				</form></td>
			</tr>
		</c:forEach>
	</table>
</body>
</html>