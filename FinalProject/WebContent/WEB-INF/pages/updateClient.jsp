<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="ctg" uri="/WEB-INF/tld/hotelTags.tld" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<fmt:setLocale value="${lang}"/>
<fmt:setBundle basename="edu.training.hotel.localization.HotelBundle"/>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title><fmt:message key="client.edit"/></title>
	<link rel="stylesheet" type="text/css" href="styles/main-style.css">
	<script language="javascript">
		function setGender(){
	 		document.getElementById("select").value = "${client.getGender()}";
	 	}
	</script>
</head>
<body onLoad="setGender()">
	<c:set var="status" scope="page" value="false"></c:set>
	<ctg:home/>
	<div class="borderForm">
		<form action="HotelServlet" method="post">
			<fmt:message key="clients.name"/>: <input type="text" name="name" value ="${client.getName()}" required>
			<fmt:message key="clients.surname"/>: <input type="text" name="surname" value ="${client.getSurname()}" required>
			<fmt:message key="clients.age"/>: <input type="number" name="age" value ="${client.getAge()}" maxlength="3" min="12" max="110" required><br>
			<fmt:message key="clients.gender"/>: <select name="gender" id="select">
					<option value="Male"><fmt:message key="client.male"/></option>
					<option value="Female"><fmt:message key="client.female"/></option>
				</select>
			<fmt:message key="clients.country"/>: <input type="text" name="country" value ="${client.getCountry()}" required>
			<fmt:message key="clients.email"/>: <input type="email" value ="${client.geteMail()}" name="email"><br>
			<input type="hidden" name="clientId" value="${client.getClientId()}">
			<button name="command" value="update_client"><fmt:message key="client.confirm"/></button>
		</form>
	</div>
</body>
</html>