<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="ctg" uri="/WEB-INF/tld/hotelTags.tld" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<fmt:setLocale value="${lang}"/>
<fmt:setBundle basename="edu.training.hotel.localization.HotelBundle"/>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>
		<fmt:message key="apartment.title"/> <c:out value="${apartment.getApartmentNumber()}"></c:out>
	</title>
	<link rel="stylesheet" type="text/css" href="styles/main-style.css">
</head>
<body>
	<ctg:home/>
	<div class="text">
		<h3><fmt:message key="apartment.info"/></h3>
		<p>
			<fmt:message key="apartment.number"/><c:out value="${apartment.getApartmentNumber()}."></c:out>
			<fmt:message key="apartment.characteristics"/><c:out value="${apartment.getFloor()}"></c:out> 
			<fmt:message key="apartment.floor"/><c:out value="${apartment.getNumberOfRooms()}"></c:out> 
			<fmt:message key="apartment.rooms"/><c:out value="${apartment.getNumberOfPlaces()}"></c:out>
			<fmt:message key="apartment.places"/>
			<c:choose>
				<c:when test="${apartment.getNumberOfBeds()==1}">
					<fmt:message key="apartment.1bed"/>
				</c:when>
				<c:otherwise>
					<fmt:message key="apartment.with"/><c:out value="${apartment.getNumberOfBeds()}"></c:out><fmt:message key="apartment.2beds"/>
				</c:otherwise>
			</c:choose>
			<br>
			<fmt:message key="apartment.type"/>
			<c:choose>
				<c:when test="${apartment.getApartmentType()== 'Standart'}">
					<fmt:message key="apartment.standart"/>.
				</c:when>
				<c:when test="${apartment.getApartmentType()== 'Balcony'}">
					<fmt:message key="apartment.balcony"/>.
				</c:when>
				<c:when test="${apartment.getApartmentType()== 'Superior'}">
					<fmt:message key="apartment.superior"/>.
				</c:when>
				<c:when test="${apartment.getApartmentType()== 'Suite'}">
					<fmt:message key="apartment.suite"/>.
				</c:when>
				<c:when test="${apartment.getApartmentType()== 'De Luxe'}">
					<fmt:message key="apartment.luxe"/>.
				</c:when>
			</c:choose>
			<br>
			<fmt:message key="apartment.cost"/><c:out value="${apartment.getCost()}$."></c:out>
			<fmt:message key="apartment.tv"/>
			<c:choose>
				<c:when test="${apartment.getAdditionalInformation().isHasTV() == true}"><fmt:message key="apartment.yes"/>, </c:when>
				<c:otherwise><fmt:message key="apartment.no"/>, </c:otherwise>
			</c:choose>
			<fmt:message key="apartment.fridge"/> - 
			<c:choose>
				<c:when test="${apartment.getAdditionalInformation().isHasFridge() == true}"><fmt:message key="apartment.yes"/>, </c:when>
				<c:otherwise><fmt:message key="apartment.no"/>, </c:otherwise>
			</c:choose>
			<fmt:message key="apartment.hasbalcony"/> - 
			<c:choose>
				<c:when test="${apartment.getAdditionalInformation().isHasBalcon() == true}"><fmt:message key="apartment.yes"/>, </c:when>
				<c:otherwise><fmt:message key="apartment.no"/>, </c:otherwise>
			</c:choose>
			<fmt:message key="apartment.conditioner"/> - 
			<c:choose>
				<c:when test="${apartment.getAdditionalInformation().isHasAirConditioner() == true}"><fmt:message key="apartment.yes"/>.</c:when>
				<c:otherwise><fmt:message key="apartment.no"/>.</c:otherwise>
			</c:choose>
		</p>
	</div>
	<c:choose>
		<c:when test="${role == 'administrator'}">
			<c:choose>
				<c:when test="${show_form == false}">
					<div class="mainform">
						<form action="HotelServlet" method="post">
							<input type="hidden" name="id" value="${apartment.getApartmentId()}">
							<input type="hidden" name="show_form" value="true">			
							<button name="command" value="show_apartment"><fmt:message key="apartment.edit"/></button>
						</form>
					</div>
				</c:when>
				<c:otherwise>
					<div class="mainform">
						<form action="HotelServlet">
							<input type="hidden" name="id" value="${apartment.getApartmentId()}">
							<button name="command" value="update_apartment"><fmt:message key="apartment.send"/></button>
							<fmt:message key="apartment.tv2"/><select name="hasTV">
								<option value="true"><fmt:message key="apartment.yes2"/></option>
								<option value="false"><fmt:message key="apartment.no2"/></option>
							</select>
							<fmt:message key="apartment.fridge2"/><select name="hasFridge">
								<option value="true"><fmt:message key="apartment.yes2"/></option>
								<option value="false"><fmt:message key="apartment.no2"/></option>
							</select>
							<fmt:message key="apartment.conditioner2"/><select name="hasAirConditioner">
								<option value="true"><fmt:message key="apartment.yes2"/></option>
								<option value="false"><fmt:message key="apartment.no2"/></option>
							</select>
						</form>
					</div>
				</c:otherwise>
			</c:choose>
		</c:when>
	</c:choose>
</body>
</html>