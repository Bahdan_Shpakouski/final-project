<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="ctg" uri="/WEB-INF/tld/hotelTags.tld" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<fmt:setLocale value="${lang}"/>
<fmt:setBundle basename="edu.training.hotel.localization.HotelBundle"/>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title><fmt:message key="history.title"/></title>
	<link rel="stylesheet" type="text/css" href="styles/main-style.css">
</head>
<body>
	<ctg:home/>
	<hr>
	<div class="mainform">
		<form action="HotelServlet">
			<fmt:message key="history.select"/>: <select name="apartmentId">
				<option value="1">101</option>
				<option value="2">102</option>
				<option value="3">103</option>
				<option value="4">104</option>
				<option value="5">105</option>
				<option value="6">106</option>
				<option value="7">201</option>
				<option value="8">202</option>
				<option value="9">203</option>
				<option value="10">204</option>
				<option value="11">205</option>
				<option value="12">206</option>
			</select>
			<button name="command" value="show_history" ><fmt:message key="history.show"/></button>
		</form>
	</div>
	<hr>
	<table  border = "1" align="center">
		<caption><fmt:message key="history.roomHistory"/><c:out value="${room}"></c:out></caption>
		<tr>
			<th>Id</th>
			<th><fmt:message key="history.moveIn"/></th>
			<th><fmt:message key="history.moveOut"/></th>
			<th><fmt:message key="clients.clientId"/></th>
			<th></th>
		</tr>
		<c:forEach items="${requestScope.list}" var="record">
			<tr>
				<td><c:out value="${record.getRecordId()}"></c:out></td>
				<td><c:out value="${record.getMovingInDate()}"></c:out></td>
				<td><c:out value="${record.getMovingOutDate()}"></c:out></td>
				<td><c:out value="${record.getClientId()}"></c:out></td>
				<td><form action="HotelServlet">
					<input type="hidden" name="clientId" value="${record.getClientId()}">
					<input type="hidden" name="mode" value="0">
					<button name="command" value="show_client"><fmt:message key="clients.clientProfile"/></button>
				</form></td>
			</tr>
		</c:forEach>
	</table>
</body>
</html>