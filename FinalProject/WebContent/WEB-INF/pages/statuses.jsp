<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="ctg" uri="/WEB-INF/tld/hotelTags.tld" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<fmt:setLocale value="${lang}"/>
<fmt:setBundle basename="edu.training.hotel.localization.HotelBundle"/>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title><fmt:message key="statuses.title"/></title>
	<link rel="stylesheet" type="text/css" href="styles/main-style.css">
	<script language="javascript">
	function setMode(mode) {
    	document.getElementById("m1").value = mode;
	}
	</script>
</head>
<body>
	<ctg:home/>
	<hr>
	<div class="mainform">
		<form action="HotelServlet">
			<input type="hidden" name="mode" id="m1" value="0">
			<button name="command" value="show_statuses" onclick="setMode(0)"><fmt:message key="statuses.current"/></button>
			<button name="command" value="show_statuses" onclick="setMode(1)"><fmt:message key="statuses.reserved"/></button>
			<button name="command" value="show_statuses" onclick="setMode(2)"><fmt:message key="statuses.expired"/></button>
		</form>
	</div>
	<hr>
	<table  border = "1" align="center">
		<caption>
			<c:choose>
				<c:when test="${mode==0}">
					<font size="6"><fmt:message key="statuses.currentCaption"/></font>
				</c:when>
				<c:when test="${mode==1}">
					<font size="6"><fmt:message key="statuses.reserved"/></font>
				</c:when>
				<c:when test="${mode==2}">
					<font size="6"><fmt:message key="statuses.expired"/></font>
				</c:when>
			</c:choose>	
		</caption>
		<tr>
			<th>Id</th>
			<th><fmt:message key="statuses.status"/></th>
			<th><fmt:message key="apartments.apartmentId"/></th>
			<th><fmt:message key="clients.clientId"/></th>
			<th><fmt:message key="bills.billId"/></th>
			<th><fmt:message key="history.moveIn"/></th>
			<th><fmt:message key="history.moveOut"/></th>
			<th></th>
			<th></th>
		</tr>
		<c:forEach items="${requestScope.list}" var="status" varStatus="statusIndex">
			<tr>
				<td><c:out value=" ${status.getStatusId()}"></c:out></td>
				<td><c:out value=" ${status.getStatus()}"></c:out></td>
				<td><c:out value=" ${status.getApartmentId()}"></c:out></td>
				<td><c:out value=" ${status.getClientId()}"></c:out></td>
				<td><c:out value=" ${status.getBillId()}"></c:out></td>
				<td><c:out value=" ${bills.get(statusIndex.index).getMovingInDate()}"></c:out></td>
				<td><c:out value=" ${bills.get(statusIndex.index).getMovingOutDate()}"></c:out></td>
				<td><form action="HotelServlet">
					<input type="hidden" name="id" value="${status.getApartmentId()}">
					<button name="command" value="show_apartment"><fmt:message key="statuses.apartmentInfo"/></button>
					<c:choose>
						<c:when test="${status.getClientId() !=0}">
							<input type="hidden" name="clientId" value="${status.getClientId()}">
							<input type="hidden" name="mode" value="0">
							<button name="command" value="show_client"><fmt:message key="clients.clientProfile"/></button>
						</c:when>
					</c:choose>
				</form></td>
				<td><form action="HotelServlet" method="post">
					<input type="hidden" name="statusId" value="${status.getStatusId()}">
					<c:choose>
						<c:when test="${status.isCurrent() == false}">
							<button name="command" value="delete_status"><fmt:message key="statuses.delete"/></button>
						</c:when>
					</c:choose>
					<c:choose>
						<c:when test="${status.getStatus() == 'Reserved' 
						&& today.equals(bills.get(statusIndex.index).getMovingInDate())}">
							<input type="hidden" name="clientId" value="${status.getClientId()}">
							<button name="command" value="set_current_status"><fmt:message key="statuses.moveIn"/></button>
						</c:when>
						<c:when test="${status.getStatus() == 'Occupied' }">
							<input type="hidden" name="clientId" value="${status.getClientId()}">
							<button name="command" value="set_expired_status"><fmt:message key="statuses.moveOut"/></button>
						</c:when>
					</c:choose>
				</form></td>
			</tr>
		</c:forEach>
	</table>	
</body>
</html>