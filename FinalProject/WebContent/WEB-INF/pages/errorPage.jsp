<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="ctg" uri="/WEB-INF/tld/hotelTags.tld" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<fmt:setLocale value="${lang}"/>
<fmt:setBundle basename="edu.training.hotel.localization.HotelBundle"/>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title><fmt:message key="error.title"/></title>
	<link rel="stylesheet" type="text/css" href="styles/main-style.css">
</head>
<body>
	<ctg:home/>
	<h1 align="center">Error 500 - Internal Server Error</h1>
	<h2 align="center"><fmt:message key="error.sorry"/></h2>
</body>
</html>