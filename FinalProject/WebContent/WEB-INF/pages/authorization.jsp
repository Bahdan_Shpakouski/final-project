<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="ctg" uri="/WEB-INF/tld/hotelTags.tld" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<fmt:setLocale value="${lang}"/>
<fmt:setBundle basename="edu.training.hotel.localization.HotelBundle"/>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title><fmt:message key="index.authorization"/></title>
	<link rel="stylesheet" type="text/css" href="styles/main-style.css">
</head>
<body>
	<ctg:home/>
	<div class="borderForm">
		<form action="HotelServlet" method="post">
			<fmt:message key="clients.name"/>: <input type="text" name="name" required>
			<fmt:message key="clients.surname"/>: <input type="text" name="surname" required><br>
			<fmt:message key="registration.password"/><input type="password" name="password" required>
			<button name="command" value="log_in"><fmt:message key="authorization.enter"/></button>
		</form>
	</div>
	<c:choose>
		<c:when test="${authoriztion_failed == true}">
			<p align="center"><font size="8"><fmt:message key="authorization.fail"/></font></p>
		</c:when>
	</c:choose>
</body>
</html>