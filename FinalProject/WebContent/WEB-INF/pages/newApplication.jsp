<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="ctg" uri="/WEB-INF/tld/hotelTags.tld" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<fmt:setLocale value="${lang}"/>
<fmt:setBundle basename="edu.training.hotel.localization.HotelBundle"/>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title><fmt:message key="newApplication.title"/></title>
	<link rel="stylesheet" type="text/css" href="styles/main-style.css">
	<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
 	<script src="//code.jquery.com/jquery-1.10.2.js"></script>
 	<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
  	<link rel="stylesheet" href="/resources/demos/style.css">
  	<script>
 	$(function() {
    	$( "#datepicker" ).datepicker({ minDate: 0, maxDate: "+6M", dateFormat: "dd-mm-yy" });
    	$( "#datepicker" ).datepicker("setDate", new Date());
  	});
 	</script>
</head>
<body>
	<ctg:home/>
	<div class="borderForm">
		<form action="HotelServlet" method="post">
			<input type="hidden" name="clientId" value="${clientId}">
			<fmt:message key="applications.beds"/>: <input type="number" name ="number_of_beds" maxlength="1" min="0" max="4"><br>
			<fmt:message key="applications.people"/>: <input type="number" name ="number_of_people"  maxlength="1" value="1" min="1" max="4" required><br>
			<fmt:message key="applications.arrival"/>: <input type="text" name="date" id="datepicker" value=""><br>
			<fmt:message key="newApplication.days"/>: <input type="number" name="number_of_days" maxlength="2" value="1" min="1" max="90" required><br>
			<fmt:message key="applications.type"/>: <select name="select_control">
	  			<option value="Standart"><fmt:message key="apartment.standart"/></option>
	  			<option value="Superior"><fmt:message key="apartment.superior"/></option>
	  			<option value="Balcony"><fmt:message key="apartment.balcony"/></option>
	  			<option value="Suite"><fmt:message key="apartment.suite"/></option>
	  			<option value="De Luxe"><fmt:message key="apartment.luxe"/></option>
			</select><br>
			<button name="command" value="create_application"><fmt:message key="newApplication.send"/></button>
		</form>
	</div>
</body>
</html>