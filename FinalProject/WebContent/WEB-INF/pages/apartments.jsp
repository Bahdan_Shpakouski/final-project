<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="ctg" uri="/WEB-INF/tld/hotelTags.tld" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<fmt:setLocale value="${lang}"/>
<fmt:setBundle basename="edu.training.hotel.localization.HotelBundle"/>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title><fmt:message key="apartments.title"/></title>
	<link rel="stylesheet" type="text/css" href="styles/list-style.css">
</head>
<body>
	<ctg:home/>
	<br>
	<h4><fmt:message key="apartments.list"/></h4>
	<ul>
		<c:forEach items="${requestScope.list}" var="apartment">
			<li>
				<fmt:message key="apartment.number"/><c:out value="${apartment.getApartmentNumber()}."></c:out>
				<fmt:message key="apartment.characteristics"/><c:out value="${apartment.getFloor()}"></c:out> 
				<fmt:message key="apartment.floor"/><c:out value="${apartment.getNumberOfRooms()}"></c:out> 
				<fmt:message key="apartment.rooms"/><c:out value="${apartment.getNumberOfPlaces()}"></c:out>
				<fmt:message key="apartment.places"/>
				<c:choose>
					<c:when test="${apartment.getNumberOfBeds()==1}">
						<fmt:message key="apartment.1bed"/>
					</c:when>
					<c:otherwise>
						<fmt:message key="apartment.with"/><c:out value="${apartment.getNumberOfBeds()}"></c:out><fmt:message key="apartment.2beds"/>
					</c:otherwise>
				</c:choose>
				<br>
				<fmt:message key="apartment.type"/>
				<c:choose>
					<c:when test="${apartment.getApartmentType()== 'Standart'}">
						<fmt:message key="apartment.standart"/>
					</c:when>
					<c:when test="${apartment.getApartmentType()== 'Balcony'}">
						<fmt:message key="apartment.balcony"/>
					</c:when>
					<c:when test="${apartment.getApartmentType()== 'Superior'}">
						<fmt:message key="apartment.superior"/>
					</c:when>
					<c:when test="${apartment.getApartmentType()== 'Suite'}">
						<fmt:message key="apartment.suite"/>
					</c:when>
					<c:when test="${apartment.getApartmentType()== 'De Luxe'}">
						<fmt:message key="apartment.luxe"/>
					</c:when>
				</c:choose>
				<br>
				<fmt:message key="apartment.cost"/><c:out value="${apartment.getCost()}$."></c:out>
				<form action="HotelServlet">
					<input type="hidden" name="id" value="${apartment.getApartmentId()}">
					<button name="command" value="show_apartment"><fmt:message key="apartments.info"/></button>
				</form>
			</li>		
			<hr>
		</c:forEach>
	</ul>
</body>
</html>