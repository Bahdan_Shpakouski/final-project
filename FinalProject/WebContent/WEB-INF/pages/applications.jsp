<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="ctg" uri="/WEB-INF/tld/hotelTags.tld" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<fmt:setLocale value="${lang}"/>
<fmt:setBundle basename="edu.training.hotel.localization.HotelBundle"/>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title><fmt:message key="applications.title"/></title>
	<link rel="stylesheet" type="text/css" href="styles/main-style.css">
</head>
<body>
	<ctg:home/>
	<hr>
	<div class="mainform">
		<form action="HotelServlet">
			<input type="hidden" name="newOnly" id ="a1" value="true">
			<button name="command" value=show_applications onclick="setShowNewApplications(false)">
				<fmt:message key="applications.allButton"/></button>
			<button name="command" value=show_applications onclick="setShowNewApplications(true)">
				<fmt:message key="applications.newButton"/></button>
		</form>
	</div>
	<hr>
	<table border = "1" align="center">
		<caption>
			<c:choose>
				<c:when test="${newOnly==true}">
					<font size="6"><fmt:message key="applications.new"/></font>
				</c:when>
				<c:otherwise>
					<font size="6"><fmt:message key="applications.all"/></font>
				</c:otherwise>
			</c:choose>	
		</caption>
		<tr>
			<th>Id</th>
			<th><fmt:message key="applications.people"/></th>
			<th><fmt:message key="applications.beds"/></th>
			<th><fmt:message key="applications.arrival"/></th>
			<th><fmt:message key="applications.days"/></th>
			<th><fmt:message key="applications.type"/></th>
			<th><fmt:message key="clients.clientId"/></th>
			<th><fmt:message key="applications.applicationDate"/></th>
			<th><fmt:message key="applications.status"/></th>
			<th></th>
		</tr>
		<c:forEach items="${requestScope.list}" var="application">
			<tr>
				<td><c:out value="${application.getApplicationId()}"></c:out></td>
				<td><c:out value="${application.getNumberOfPeople()}"></c:out></td>
				<td><c:out value="${application.getNumberOfBeds()}"></c:out></td>
				<td><c:out value="${application.getArrivalDate()}"></c:out></td>
				<td><c:out value="${application.getNumberOfDays()}"></c:out></td>
				<td><c:out value="${application.getAppartmentType()}"></c:out></td>
				<td><c:out value="${application.getClientId()}"></c:out></td>
				<td><c:out value="${application.getApplicationDate()}"></c:out></td>
				<td><c:out value="${application.getApplicationStatus()}"></c:out></td>
				<td><form action="HotelServlet" method="post">
					<c:choose>
						<c:when test="${application.getApplicationStatus()=='New'}">
								<input type="hidden" name="applicationId" 
									value="${application.getApplicationId()}">
								<button name="command" value="serve_application">
									<fmt:message key="applications.serve"/></button>
						</c:when>
					</c:choose>	
					<input type="hidden" name="clientId" value="${application.getClientId()}">
					<input type="hidden" name="mode" value="0">
					<button name="command" value="show_client"><fmt:message key="clients.clientProfile"/></button>
				</form></td>
			</tr>
		</c:forEach>
	</table>
	<script language="javascript">
	function setShowNewApplications(value) {
    	document.getElementById("a1").value = value;
	}
	</script>
</body>
</html>