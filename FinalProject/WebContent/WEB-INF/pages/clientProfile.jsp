<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="ctg" uri="/WEB-INF/tld/hotelTags.tld" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<fmt:setLocale value="${lang}"/>
<fmt:setBundle basename="edu.training.hotel.localization.HotelBundle"/>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link rel="stylesheet" type="text/css" href="styles/main-style.css">
	<title><fmt:message key="clients.clientProfile"/> № ${client.getClientId()}</title>
	<script language="javascript">
	function checkPassword() {
		 var password1 = document.getElementById("pass1").value;
		 var password2 = document.getElementById("pass2").value;
		 if(password1 == password2) {
			 document.getElementById("password_status").innerHTML = "";        
		 }else {
			 document.getElementById("password_status").innerHTML = "<fmt:message key='registration.passwordMessage'/>";  
		 }
	}
	function setMode(mode) {
    	document.getElementById("m1").value = mode;
	}
	function setSomeValue() {
		document.getElementById("pass1").value = 1;
		document.getElementById("pass2").value = 1;
	}
	</script>
</head>
<body>
	<c:choose>
		<c:when test="${show_form == null}">
			<c:set var="show_form" scope="request" value="false"></c:set>
		</c:when>
	</c:choose>
	<ctg:home/>
	<hr>
	<div class="mainform">
		<form action="HotelServlet" method="post">
			<input type="hidden" name="clientId" value="${client.getClientId()}">
			<input type="hidden" name="mode" id="m1" value="0">
			<button name="command" value="show_client" onclick="setMode(0)"><fmt:message key="client.info"/></button>
			<button name="command" value="show_client" onclick="setMode(1)"><fmt:message key="client.applications"/></button>
			<button name="command" value="show_client" onclick="setMode(2)"><fmt:message key="client.bills"/></button>
		</form>
	</div>
	<hr>
	<c:choose>
		<c:when test="${mode == 0 }">
			<h3 align="center"><fmt:message key="client.info"/></h3>
			<p align="center"><fmt:message key="clients.name"/>: <c:out value="${client.getName()},"></c:out>
			<fmt:message key="clients.surname2"/>: <c:out value="${client.getSurname()},"></c:out>
			<fmt:message key="clients.age2"/>: <c:out value="${client.getAge()},"></c:out>
			<fmt:message key="clients.gender2"/>: <c:out value="${client.getGender()},"></c:out>
			<fmt:message key="clients.country2"/>: <c:out value="${client.getCountry()},"></c:out>
			<fmt:message key="clients.email2"/>: <c:out value="${client.geteMail()}."></c:out></p><br>
			<c:choose>
				<c:when test="${role == 'client'}">
				<div class="mainform">
						<form action="HotelServlet" method="post">
							<input type="hidden" name="clientId" value="${client.getClientId()}">
							<input type="hidden" name="mode" id="m1" value="0">
							<button name="command" value="show_update_client_form"><fmt:message key="client.edit"/></button>
							<c:choose>
								<c:when test="${show_form == false}">
									<input type="hidden" name="show_form" value="true">			
									<button name="command" value="show_client"><fmt:message key="client.changePassword"/></button>
								</c:when>
								<c:otherwise>
									<fmt:message key="registration.password"/><input type="password" name="password"
										 id="pass1" onChange="checkPassword()" required>
									<fmt:message key="registration.password2"/><input type="password" 
										id="pass2" onChange="checkPassword()" required><br>
									<p id="password_status"></p>
									<button name="command" value="update_client_password"><fmt:message key="client.change"/></button>
									<button name="command" value="show_client" onClick="setSomeValue()">
										<fmt:message key="client.cancel"/></button>
								</c:otherwise>
							</c:choose>
						</form>
					</div>
				</c:when>
			</c:choose>
		</c:when>
		<c:when test="${mode == 1 }">
			<table border = "1" align="center">
				<caption><font size="6"><fmt:message key="client.applications"/></font></caption>
				<tr>
					<th>Id</th>
					<th><fmt:message key="applications.people"/></th>
					<th><fmt:message key="applications.beds"/></th>
					<th><fmt:message key="applications.arrival"/></th>
					<th><fmt:message key="applications.days"/></th>
					<th><fmt:message key="applications.type"/></th>
					<th><fmt:message key="applications.applicationDate"/></th>
					<th><fmt:message key="applications.status"/></th>
					<th></th>
				</tr>
				<c:forEach items="${requestScope.applicationList}" var="application">
					<tr>
						<td><c:out value="${application.getApplicationId()}"></c:out></td>
						<td><c:out value="${application.getNumberOfPeople()}"></c:out></td>
						<td><c:out value="${application.getNumberOfBeds()}"></c:out></td>
						<td><c:out value="${application.getArrivalDate()}"></c:out></td>
						<td><c:out value="${application.getNumberOfDays()}"></c:out></td>
						<td><c:out value="${application.getAppartmentType()}"></c:out></td>
						<td><c:out value="${application.getApplicationDate()}"></c:out></td>
						<td><c:out value="${application.getApplicationStatus()}"></c:out></td>
						<td><c:choose>
							<c:when test="${role == 'client'}">
								<form action="HotelServlet" method="post">
								<input type="hidden" name="applicationId" value="${application.getApplicationId()}">
								<input type="hidden" name="clientId" value="${client.getClientId()}">
								<c:choose>
									<c:when test="${application.getApplicationStatus()=='New'}">
										<button name="command" value="show_update_application_form">
											<fmt:message key="client.editApplication"/></button>
									</c:when>
								</c:choose>	
								<button name="command" value = "delete_application"><fmt:message key="client.delete"/></button>
								</form>
							</c:when>
						</c:choose></td>
					</tr>
				</c:forEach>
			</table>
			<c:choose>
				<c:when test="${role == 'client'}">
					<div class="mainform">
						<form action="HotelServlet" method="post">
							<input type="hidden" name="clientId" value="${client.getClientId()}">
							<button name="command" value="show_application_form"><fmt:message key="index.newApplication"/></button>
						</form>	
					</div>
				</c:when>
			</c:choose>
		</c:when>
		<c:when test="${mode == 2 }">
			<table border = "1" align="center">
				<caption><font size="6"><fmt:message key="client.bills"/></font></caption>
				<tr>
					<th>Id</th>
					<th><fmt:message key="bills.cost"/></th>
					<th><fmt:message key="applications.arrival"/></th>
					<th><fmt:message key="bills.departure"/></th>
					<th><fmt:message key="bills.status"/></th>
					<th><fmt:message key="apartments.apartmentId"/></th>
				</tr>
				<c:forEach items="${requestScope.billList}" var="bill">
					<tr>
						<td><c:out value="${bill.getBillId()}"></c:out></td>
						<td><c:out value="${bill.getCost()}"></c:out></td>
						<td><c:out value="${bill.getMovingInDate()}"></c:out></td>
						<td><c:out value="${bill.getMovingOutDate()}"></c:out></td>
						<td><c:out value="${bill.getBillStatus()}"></c:out></td>
						<td><c:out value="${bill.getApartmentId()}"></c:out></td>
						<c:choose>
							<c:when test="${role == 'client'}">
								<c:choose>
									<c:when test="${bill.getBillStatus()=='New'}">
										<td><form action="HotelServlet" method="post">
											<input type="hidden" name="billId" value="${bill.getBillId()}">
											<button name="command" value="pay_bill"><fmt:message key="client.payBill"/></button>
											<button name="command" value="decline_bill"><fmt:message key="client.declineBill"/></button>
										</form></td>
									</c:when>
								</c:choose>
							</c:when>
						</c:choose>
					</tr>
				</c:forEach>
			</table>
		</c:when>
	</c:choose>
</body>
</html>