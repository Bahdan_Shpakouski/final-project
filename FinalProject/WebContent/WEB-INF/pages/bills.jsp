<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="ctg" uri="/WEB-INF/tld/hotelTags.tld" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<fmt:setLocale value="${lang}"/>
<fmt:setBundle basename="edu.training.hotel.localization.HotelBundle"/>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title><fmt:message key="bills.title"/></title>
	<link rel="stylesheet" type="text/css" href="styles/main-style.css">
</head>
<body>
	<ctg:home/>
	<hr>
	<div class="mainform">
		<form action="HotelServlet">
			<input type="hidden" name="newOnly" id ="a1" value="true">
			<button name="command" value=show_bills onclick="setShowNewBills(false)">
				<fmt:message key="bills.allButton"/></button>
			<button name="command" value=show_bills onclick="setShowNewBills(true)">
				<fmt:message key="bills.newButton"/></button>
		</form>
	</div>
	<hr>
	<table border = "1" align="center">
		<caption>
			<c:choose>
				<c:when test="${newOnly==true}">
					<font size="6"><fmt:message key="bills.new"/></font>
				</c:when>
				<c:otherwise>
					<font size="6"><fmt:message key="bills.all"/></font>
				</c:otherwise>
			</c:choose>	
		</caption>
		<tr>
			<th>Id</th>
			<th><fmt:message key="bills.cost"/></th>
			<th><fmt:message key="applications.arrival"/></th>
			<th><fmt:message key="bills.departure"/></th>
			<th><fmt:message key="bills.status"/></th>
			<th><fmt:message key="clients.clientId"/></th>
			<th><fmt:message key="apartments.apartmentId"/></th>
			<th></th>
		</tr>
		<c:forEach items="${requestScope.list}" var="bill">
			<tr>
				<td><c:out value="${bill.getBillId()}"></c:out></td>
				<td><c:out value="${bill.getCost()}"></c:out></td>
				<td><c:out value="${bill.getMovingInDate()}"></c:out></td>
				<td><c:out value="${bill.getMovingOutDate()}"></c:out></td>
				<td><c:out value="${bill.getBillStatus()}"></c:out></td>
				<td><c:out value="${bill.getClientId()}"></c:out></td>
				<td><c:out value="${bill.getApartmentId()}"></c:out></td>
				<td><form action="HotelServlet" method="post">
					<input type="hidden" name="clientId" value="${bill.getClientId()}">
					<input type="hidden" name="mode" value="0">
					<button name="command" value="show_client"><fmt:message key="clients.clientProfile"/></button>
					<c:choose>
						<c:when test="${bill.getBillStatus() == 'Payed'}">
							<input type="hidden" name="billId" value="${bill.getBillId()}">
							<button name="command" value="confirm_bill"><fmt:message key="bills.confirm"/></button>
						</c:when>
					</c:choose>
				</form></td>
			</tr>
		</c:forEach>
	</table>
	<script language="javascript">
	function setShowNewBills(value) {
    	document.getElementById("a1").value = value;
	}
	</script>
</body>
</html>