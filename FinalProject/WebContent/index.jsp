<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<c:choose>
	<c:when test="${lang == null }">
		<c:set var="lang" scope="session" value="ru_RU"></c:set>
	</c:when>
</c:choose>
<fmt:setLocale value="${lang}"/>
<fmt:setBundle basename="edu.training.hotel.localization.HotelBundle"/>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title><fmt:message key="index.title"/></title>
	<link rel="stylesheet" type="text/css" href="styles/main-style.css">
</head>
<body>
	<h1 align="center"><fmt:message key="index.greeting"/></h1>
	<div class="localeButton">
		<form action="HotelServlet" method="get">
			<select name="lang">
				<option value="en_US" ${lang== 'en_US' ? 'selected' : ''}>English</option>
				<option value="ru_RU" ${lang== 'ru_RU' ? 'selected' : ''}>Русский</option>
			</select>
			<button name="command" value="change_locale"><fmt:message key="index.lang"/></button>
		</form>
	</div>
	<c:choose>
		<c:when test="${role == null}">
			<c:set var="role" scope="session" value="guest"></c:set>
		</c:when>
	</c:choose>
	<hr>
	<div class="mainform">
		<form action="HotelServlet" method="post">
			<button name="command" value="show_apartments"><fmt:message key="index.apartments"/></button>
			<c:choose>
				<c:when test="${role == 'administrator'}">
					<button name="command" value="show_clients"><fmt:message key="index.clients"/></button>
					<input type="hidden" name="newOnly" value="true">
					<button name="command" value=show_applications><fmt:message key="index.applications"/></button>
					<button name="command" value="show_bills"><fmt:message key="index.bills"/></button>
					<input type="hidden" name="apartmentId" value="1">
					<button name="command" value="show_history"><fmt:message key="index.history"/></button>
					<button name="command" value="show_statuses"><fmt:message key="index.statuses"/></button>
				</c:when>
				<c:when test="${role == 'client'}">
					<input type="hidden" name="clientId" value="${sessionScope.clientId}">
					<button name="command" value="show_client"><fmt:message key="index.clientProfile"/></button>
					<button name="command" value="show_application_form"><fmt:message key="index.newApplication"/></button>
				</c:when>
				<c:when test="${role == 'guest'}">
					<button name="command" value="show_authorization_form"><fmt:message key="index.authorization"/></button>
					<button name="command" value="show_client_form"><fmt:message key="index.registration"/></button>
				</c:when>
			</c:choose>
			<c:choose>
				<c:when test="${role != 'guest'}">
					<input type="hidden" name="mode" value="0">
					<button name="command" value="log_out"><fmt:message key="index.exit"/></button>
				</c:when>
			</c:choose>
		</form>
	</div>
	<hr>
	<div class="footer"><h2><fmt:message key="index.enter"/><c:out value="${role}"></c:out></h2></div>
</body>
</html>